import datetime
import functools
import json
import operator
import pytz

from django.contrib import messages
from django.db import connection, reset_queries, transaction
from django.db.models import Q
from django.http import HttpResponse, HttpResponseBadRequest
from django.shortcuts import redirect
from django.urls import reverse
from django.utils import timezone

import data.manual as manual_data
from core.models import Currency, GarrisonFollower, Item
from core.util import *
from thing.models import *
from thing.models.characterreputation import _level_modifiers

# ---------------------------------------------------------------------------

BAG_IDS = [0, 1, 2, 3, 4]
BANK_IDS = [-1, 5, 6, 7, 8, 9, 10, 11]
REAGENT_IDS = [-3]
VOID_IDS = [1, 2]

COOL_PEOPLE = set([
    'freddie', 'hestiah', 'alcaras', 'Glayde', 'kenlyric', # US
    'rubert', # EU
])
VALID_MOUNTS = [229376, 229377, 229417, 232412]
VALID_PETS = [33239, 73809]
VALID_QUESTS = list(range(43646, 43682))
VALID_REPUTATIONS = [1492]
VALID_PARAGONS = [1828, 1859, 1883, 1894, 1900, 1948, 2045, 2165, 2170]
MAX_VALID_CRUCIBLE = 1784

MIN_ADDON_VERSION = 14

# ---------------------------------------------------------------------------

class UploadHandler(object):
    def __init__(self, request, data):
        self.request = request
        self.data = data

        self._all_items = []
        self._global_world_quests = {}
        self._new_cooldowns = []
        self._new_currencies = []
        self._new_followers = []
        self._new_lockouts = []
        self._new_missions = []
        self._new_pets = []
        self._new_reputations = []
        self._new_world_quests = []

        self.tt = TimerThing('UploadHandler')
        self.tt.add_time('start')

    # -----------------------------------------------------------------------

    def run(self):
        reset_queries()

        # Check data version
        version = self.data['lua_file'].get('version', 0)
        if version < MIN_ADDON_VERSION:
            return self.response(messages.ERROR, 'Addon version too old.')

        # Make sure we have some characters
        try:
            self.characters = self.data['lua_file'].get('chars', {}).items()
        except AttributeError:
            return self.response(messages.ERROR, 'Invalid Lua data file.')

        # Load global world quests
        for gwq in GlobalWorldQuest.objects.all():
            self._global_world_quests[gwq.region] = dict(obj=gwq, factions=gwq.factions[:], expires=gwq.expires[:])

        self.tt.add_time('init')

        # Generate character filters
        characters = {}
        filters = []
        for char_name, char_data in self.characters:
            if char_data.get('lastSeen') is None:
                print('Never seen: %r' % (char_name))
                continue

            # Region - Realm - Character
            parts = char_name.split(' - ')
            if len(parts) != 3:
                print('Bad char_name: %r' % (char_name))
                continue

            char_data['_region'] = parts[0].lower()

            realm = manual_data.realm_map.get((char_data['_region'], parts[1]))
            if realm:
                char_scanned = self.utctimestamp(char_data.get('lastSeen'))
                filters.append(
                    Q(name=parts[2], realm=realm.id)
                    &
                    (Q(last_char_update__isnull=True) | Q(last_char_update__lt=char_scanned))
                )
            else:
                print('Bad realm: %r/%r' % (parts[0], parts[1]))

            # Check if this data is newer
            key = char_name.lower()
            current_data = characters.get(key)
            if current_data is None or char_data.get('lastSeen', 0) > current_data.get('lastSeen', 0):
                characters[key] = char_data

        self.characters = characters

        if not filters:
            return self.response(messages.ERROR, 'No valid characters in data.')

        # Fetch character data
        self.fetch_character_data(filters)

        self.tt.add_time('char filters')

        # And off we go
        account = None
        for char_name, char_data in self.characters.items():
            # Make sure we found this character in the database
            character = self.character_map.get(char_name)
            if character is not None:
                self.update_character(character, char_data)
                account = character.bnetaccount

        self.tt.add_time('char loop')

        # Create any new objects
        AccountPet.objects.bulk_create(self._new_pets)
        CharacterCooldown.objects.bulk_create(self._new_cooldowns)
        CharacterCurrency.objects.bulk_create(self._new_currencies)
        CharacterFollower.objects.bulk_create(self._new_followers)
        CharacterLockout.objects.bulk_create(self._new_lockouts)
        #CharacterMission.objects.bulk_create(self._new_missions)
        CharacterReputation.objects.bulk_create(self._new_reputations)
        CharacterWorldQuest.objects.bulk_create(self._new_world_quests)

        self.tt.add_time('bulk create')

        # Finish updating items
        self.update_items()

        self.tt.add_time('update items')

        # Update toys
        if account:
            toys = self.data['lua_file'].get('toys', [])
            if len(toys) > 0:
                if account.toys is None:
                    account.toys = []

                if isinstance(toys, dict):
                    new_toys = sorted(t for t in toys.keys() if t in manual_data.all_toys)
                else:
                    new_toys = sorted(toys)

                new_toys = list(set(new_toys) | set(account.toys))
                if new_toys != account.toys:
                    account.toys = new_toys
                    account.save(update_fields=['toys'])

            self.tt.add_time('update toys')

        # Update global world quests
        for region, data in self._global_world_quests.items():
            if data['obj'].factions != data['factions'] or data['obj'].expires != data['expires']:
                data['obj'].factions = data['factions']
                data['obj'].expires = data['expires']
                data['obj'].save()

        # Done
        self.tt.finished()
        return self.response(messages.SUCCESS, 'Upload successful.')

    # -----------------------------------------------------------------------

    def response(self, status, message):
        messages.add_message(self.request, status, message)
        return redirect(reverse('upload'))

    def utctimestamp(self, t):
        return datetime.datetime.utcfromtimestamp(t).replace(tzinfo=pytz.utc)

    # -----------------------------------------------------------------------

    def fetch_character_data(self, filters):
        self.character_map = {}

        # Build a character query set
        character_qs = Character.objects.filter(
            bnetaccount__user=self.request.user,
        ).filter(
            functools.reduce(operator.or_, filters),
        ).prefetch_related(
            'bnetaccount',
        )

        self.account_pets = {}

        account = None
        temp_map = {}
        for character in character_qs:
            character.cooldown_map = {}
            character.currency_map = {}
            character.follower_map = {}
            character.lockout_map = {}
            character.mission_map = {}
            character.reputation_map = {}
            character.world_quest_map = {}
            character._items = []

            realm = manual_data.realm_map[character.realm_id]
            character._realm = realm
            key = '%s - %s - %s' % (realm.region, realm.name, character.name)
            self.character_map[key.lower()] = character
            temp_map[character.id] = character

            account = character.bnetaccount

        self.tt.add_time('character map')

        # Account pets
        ap_qs = AccountPet.objects.filter(
            bnetaccount=account,
        )
        for ap in ap_qs:
            self.account_pets[ap.guid] = ap

        # Cooldowns
        cc_qs = CharacterCooldown.objects.filter(
            character__in=temp_map.keys(),
        )
        for cc in cc_qs:
            temp_map[cc.character_id].cooldown_map[cc.spell_id] = cc

        self.tt.add_time('character cooldowns')

        # Currencies
        cc_qs = CharacterCurrency.objects.filter(
            character__in=temp_map.keys(),
        )
        for cc in cc_qs:
            temp_map[cc.character_id].currency_map[cc.currency_id] = cc

        self.tt.add_time('character currencies')

        # Followers
        cf_qs = CharacterFollower.objects.filter(
            character__in=temp_map.keys(),
        )
        for cf in cf_qs:
            temp_map[cf.character_id].follower_map[cf.follower_id] = cf

        self.tt.add_time('character followers')

        # Lockouts
        cl_qs = CharacterLockout.objects.filter(
            character__in=temp_map.keys(),
        )
        for cl in cl_qs:
            temp_map[cl.character_id].lockout_map[(cl.instance_id, cl.difficulty)] = cl

        self.tt.add_time('character lockouts')

        # Missions
        # cm_qs = CharacterMission.objects.filter(
        #     character__in=temp_map.keys(),
        # )
        # for cm in cm_qs:
        #     temp_map[cm.character_id].mission_map[cm.mission_id] = cm

        # self.tt.add_time('character missions')

        # Reputations
        cr_qs = CharacterReputation.objects.filter(
            character__in=temp_map.keys(),
            faction__in=VALID_REPUTATIONS + VALID_PARAGONS,
        )
        for cr in cr_qs:
            temp_map[cr.character_id].reputation_map[cr.faction_id] = cr

        self.tt.add_time('character reputations')

        # World Quests
        cwq_qs = CharacterWorldQuest.objects.filter(
            character__in=temp_map.keys(),
        )
        for cwq in cwq_qs:
            temp_map[cwq.character_id].world_quest_map[cwq.expires] = cwq

        self.tt.add_time('character world quests')

        # Items
        ci_qs = CharacterItem.objects.filter(
            character__in=temp_map.keys(),
        ).exclude(
            location=CharacterItem.EQUIPPED_LOCATION,
        )
        for ci in ci_qs:
            temp_map[ci.character_id]._items.append(ci)

        self.tt.add_time('character items')

    # -----------------------------------------------------------------------

    def update_character(self, character, char_data):
        # Update basic information
        character.last_char_update = self.utctimestamp(char_data.get('lastSeen'))
        self._update_fields = ['last_char_update']

        keystone_id = int(char_data.get('keystoneInstance', 0))

        honor = char_data.get('honor', {})

        self._update_fields.extend(update_fields(
            character,
            dict(
                copper=char_data.get('copper', 0),
                flight_speed=char_data.get('flightSpeed', 0),
                ground_speed=char_data.get('groundSpeed', 0),
                played_total=char_data.get('playedTotal', 0),
                played_level=char_data.get('playedLevel', 0),
                current_xp=char_data.get('currentXP', 0),
                level_xp=char_data.get('levelXP', 0),
                rested_xp=char_data.get('restedXP', 0),
                resting=char_data.get('resting', 0) is True,
                pvp_prestige=honor.get('prestige', 0),
                pvp_level=honor.get('level', 1),
                pvp_honor_current=honor.get('current', 0),
                pvp_honor_max=honor.get('max', 350),
                keystone_id=keystone_id if keystone_id in manual_data.keystone_maps else 0,
                keystone_level=char_data.get('keystoneLevel', 0),
                #keystone_max=char_data.get('keystoneMax', 0),
                hidden_dungeons=char_data.get('hiddenDungeons', 0),
                hidden_kills=char_data.get('hiddenKills', 0),
                hidden_world_quests=char_data.get('hiddenWorldQuests', 0),
                balance_mythic15=char_data.get('balanceMythic15', False),
                balance_unleashed_monstrosities=[n for n in char_data.get('balanceUnleashedMonstrosities', []) if isinstance(n, int) and n <= 11],
                bigger_fish_to_fry=[n for n in char_data.get('legionRareFish', []) if isinstance(n, int) and n <= 18],
                azerite_level=char_data.get('azeriteLevel', 0),
                azerite_current_xp=char_data.get('azeriteCurrentXP', 0),
                azerite_level_xp=char_data.get('azeriteLevelXP', 0),
            ),
        ))

        self.scan_times = char_data.get('scanTimes', {})

        self.update_character_cooldowns(character, char_data)
        self.update_character_currencies(character, char_data)
        self.update_character_items(character, char_data)
        self.update_character_lockouts(character, char_data)
        self.update_character_mounts(character, char_data)
        #self.update_character_order_hall_talents(character, char_data)
        self.update_character_pets(character, char_data)
        self.update_character_quests(character, char_data)
        self.update_character_reputations(character, char_data)
        self.update_character_world_quests(character, char_data)

        self.update_character_followers(character, char_data)
        #self.update_character_missions(character, char_data)

        cache_scanned = self.scan_times.get('bags')
        if cache_scanned:
            cache_scanned = self.utctimestamp(cache_scanned)
            if character.last_cache_check is None or character.last_cache_check <= cache_scanned:
                character.last_cache_check = cache_scanned
                self._update_fields.append('last_cache_check')


        # Save updated character information
        character.save(update_fields=self._update_fields)

    # -----------------------------------------------------------------------

    def update_character_cooldowns(self, character, char_data):
        tradeskills = char_data.get('tradeSkills', {})
        if not isinstance(tradeskills, dict):
            return

        for spell_id, timestamp in tradeskills.items():
            if spell_id not in manual_data.all_cooldowns:
                print('Invalid cooldown spellID: %s' % (spell_id))
                continue

            expires = self.utctimestamp(timestamp)
            cooldown = character.cooldown_map.get(spell_id)
            if cooldown is None:
                self._new_cooldowns.append(CharacterCooldown(
                    character=character,
                    spell_id=spell_id,
                    expires=expires,
                ))
            elif cooldown.expires is None or expires > cooldown.expires:
                cooldown.expires = expires
                cooldown.save(update_fields=['expires'])

        self.tt.add_time('loop cooldowns')

    # -----------------------------------------------------------------------

    def update_character_currencies(self, character, char_data):
        # Update currencies
        for currency_id, currency_data in char_data.get('currencies', {}).items():
            currency_id = int(currency_id)
            if currency_id not in manual_data.currency_map:
                continue

            amount, totalMax, earnedThisWeek, weeklyMax = currency_data
            # TODO: check if week has passed and data is old?

            currency = character.currency_map.get(currency_id)
            if currency is None:
                self._new_currencies.append(CharacterCurrency(
                    character=character,
                    currency_id=currency_id,
                    total=amount,
                    total_max=totalMax,
                    week=earnedThisWeek,
                    week_max=weeklyMax,
                ))
            else:
                updated_fields = update_fields(
                    currency,
                    dict(
                        total=amount,
                        total_max=totalMax,
                        week=earnedThisWeek,
                        week_max=weeklyMax,
                    ),
                )
                if updated_fields:
                    currency.save(update_fields=updated_fields)

        self.tt.add_time('loop currencies')

    # -----------------------------------------------------------------------

    def update_character_items(self, character, char_data):
        # Bags
        bags_scanned = self.scan_times.get('bags')
        if bags_scanned:
            bags_scanned = self.utctimestamp(bags_scanned)
            if character.last_bag_update is None or character.last_bag_update <= bags_scanned:
                character.last_bag_update = bags_scanned
                self._update_fields.append('last_bag_update')

                self._do_items(
                    character,
                    char_data,
                    CharacterItem.BAGS_LOCATION,
                    BAG_IDS,
                    'bag %d',
                )

        # Bank and reagent bank
        bank_scanned = self.scan_times.get('bank')
        if bank_scanned:
            bank_scanned = self.utctimestamp(bank_scanned)
            if character.last_bank_update is None or character.last_bank_update <= bank_scanned:
                character.last_bank_update = bank_scanned
                self._update_fields.append('last_bank_update')

                self._do_items(
                    character,
                    char_data,
                    CharacterItem.BANK_LOCATION,
                    BANK_IDS,
                    'bag %d',
                )

                self._do_items(
                    character,
                    char_data,
                    CharacterItem.REAGENT_BANK_LOCATION,
                    REAGENT_IDS,
                    'bag %d',
                )

        # Void storage
        void_scanned = self.scan_times.get('void')
        if void_scanned:
            void_scanned = self.utctimestamp(void_scanned)
            if character.last_void_update is None or character.last_void_update <= void_scanned:
                character.last_void_update = void_scanned
                self._update_fields.append('last_void_update')

                self._do_items(
                    character,
                    char_data,
                    CharacterItem.VOID_STORAGE_LOCATION,
                    VOID_IDS,
                    'void %d',
                )

        self.tt.add_time('loop items')

    def _do_items(self, character, char_data, location, bag_ids, bag_format):
        for bagID in bag_ids:
            items = char_data.get('items', {}).get(bag_format % bagID, {})

            if not isinstance(items, dict):
                print(location, bagID, 'not dict')
                return

            if location == 2 and bagID == 4:
                print('what', character.name)

            for k, v in items.items():
                if k.startswith('s'):
                    slot = k[1:]
                    if not (isinstance(v, list) and len(v) >= 2):
                        print(location, bagID, 'check 1')
                        continue

                    count, itemID = v[:2]
                    if not (slot.isdigit() and isinstance(count, int) and isinstance(itemID, int)):
                        print(location, bagID, 'check 2')
                        continue

                    quality = -1
                    if len(v) >= 3 and isinstance(v[2], int):
                        quality = v[2]

                    extra = dict()
                    if len(v) >= 4 and isinstance(v[3], dict):
                        bonuses = [b for k, b in v[3].items() if k.startswith('bonus')]
                        if bonuses:
                            extra['b'] = bonuses

                        if 'enchant' in v[3]:
                            extra['e'] = v[3]['enchant']

                        gems = [g for k, g in v[3].items() if k.startswith('gem')]
                        if gems:
                            extra['g'] = ':'.join(str(g) for g in gems)

                    self._all_items.append([
                        character,
                        itemID,
                        count,
                        location,
                        bagID,
                        int(slot),
                        quality,
                        extra,
                    ])

                elif k == 'bagItemID':
                    if not isinstance(v, int):
                        continue

                    self._all_items.append([
                        character,
                        v,
                        1,
                        location,
                        bagID,
                        0,
                        -1,
                        {},
                    ])

    # -----------------------------------------------------------------------

    def update_character_lockouts(self, character, char_data):
        lockouts = char_data.get('lockouts', [])

        # Make a fake lockout for invasions
        weekly_quests = char_data.get('weeklyQuests', {})
        if weekly_quests.get(37638) or weekly_quests.get(37639) or weekly_quests.get(37640):
            if weekly_quests.get(37640):
                bosses = [['Bronze', True], ['Silver', True], ['Gold', True]]
                defeated = 3

            elif weekly_quests.get(37639):
                bosses = [['Bronze', True], ['Silver', True], ['Gold', False]]
                defeated = 2

            elif weekly_quests.get(37638):
                bosses = [['Bronze', True], ['Silver', False], ['Gold', False]]
                defeated = 1

            lockouts.append(dict(
                difficulty=0,
                name='Garrison Invasion',
                expires=self.get_weekly_reset(character._realm),
                locked=True,
                defeatedBosses=defeated,
                maxBosses=3,
                bosses=bosses,
            ))

        keep_fakes = []
        for lockout_data in lockouts:
            instance = manual_data.instance_map.get(lockout_data['name'])
            if instance is None:
                #print('Invalid instance name: ', lockout_data['name'])
                continue

            # weekly quests are weird
            if lockout_data.get('weeklyQuest') is True:
                expires = self.get_weekly_reset(character._realm)

            else:
                expires = lockout_data.get('expires')
                if expires is None:
                    # round to the nearest minute first
                    reset_time = lockout_data['resetTime']
                    if reset_time == 0:
                        expires = None
                    else:
                        rem = lockout_data['resetTime'] % 60
                        if rem > 30:
                            reset_time += (60 - rem)
                        elif rem < 30:
                            reset_time -= rem

                        expires = self.utctimestamp(reset_time)

            # generate the boss array
            bosses = []
            for thing in lockout_data.get('bosses', []):
                if type(thing) is list and len(thing) == 2:
                    boss_name, boss_killed = thing
                    bosses.append('%s|%s' % (boss_name.replace('<', '').replace('>', '')[:38], boss_killed and 1 or 0))

            locked = lockout_data.get('locked', True)

            lockout = character.lockout_map.get((instance.id, lockout_data['difficulty']))
            if lockout is None:
                self._new_lockouts.append(CharacterLockout(
                    character=character,
                    instance=instance,
                    bosses=bosses,
                    bosses_killed=lockout_data['defeatedBosses'],
                    bosses_max=lockout_data['maxBosses'],
                    difficulty=lockout_data['difficulty'],
                    expires=expires,
                    locked=locked,
                ))
            else:
                updated_fields = update_fields(
                    lockout,
                    dict(
                        bosses=bosses,
                        bosses_killed=lockout_data['defeatedBosses'],
                        expires=expires,
                        locked=locked,
                    ),
                )
                if updated_fields:
                    lockout.save(update_fields=updated_fields)

            if lockout_data['difficulty'] == 0:
                keep_fakes.append(instance.id)

        CharacterLockout.objects.filter(character=character, difficulty=0).exclude(instance__in=keep_fakes).delete()

        self.tt.add_time('loop lockouts')

    # -----------------------------------------------------------------------

    def update_character_mounts(self, character, char_data):
        # Mounts have been scanned?
        mounts_scanned = self.scan_times.get('mounts')
        if not mounts_scanned:
            return

        # Mounts have been scanned since last time?
        mounts_scanned = self.utctimestamp(mounts_scanned)
        if character.last_mount_update is not None and character.last_mount_update >= mounts_scanned:
            return

        character.last_mount_update = mounts_scanned
        self._update_fields.append('last_mount_update')

        valid_mounts = list(sorted(m for m in char_data.get('mounts', []) if m in VALID_MOUNTS))
        cd = CharacterData.objects.only('mounts_extra').get(character=character)
        if cd.mounts_extra != valid_mounts:
            cd.mounts_extra = valid_mounts
            cd.save(update_fields=['mounts_extra'])

        self.tt.add_time('loop mounts')

    # -----------------------------------------------------------------------

    def update_character_pets(self, character, char_data):
        # Pets have been scanned?
        pets_scanned = self.scan_times.get('pets')
        if not pets_scanned:
            return

        # Pets have been scanned since last time?
        pets_scanned = self.utctimestamp(pets_scanned)
        if character.last_pet_update is not None and character.last_pet_update >= pets_scanned:
            return

        character.last_pet_update = pets_scanned
        self._update_fields.append('last_pet_update')

        pets = char_data.get('pets')
        if pets:
            for pet_data in pets:
                if pet_data['petID'] not in VALID_PETS:
                    continue

                guid = pet_data['guid'].split('-')[-1].zfill(16)

                ap = self.account_pets.get(guid)
                if ap is None:
                    self._new_pets.append(AccountPet(
                        bnetaccount=character.bnetaccount,
                        pet_id=pet_data['petID'],
                        favourite=pet_data['favourite'],
                        guid=guid,
                        level=pet_data['level'],
                        name=pet_data['name'],
                        quality=pet_data['quality'],
                    ))
                else:
                    updated_fields = update_fields(
                        ap,
                        dict(
                            pet_id=pet_data['petID'],
                            favourite=pet_data['favourite'],
                            level=pet_data['level'],
                            name=pet_data['name'],
                            quality=pet_data['quality'],
                        )
                    )
                    if updated_fields:
                        ap.save(update_fields=updated_fields)

        self.tt.add_time('loop pets')

    # -----------------------------------------------------------------------

    def update_character_order_hall_talents(self, character, char_data):
        talents = [[t['id'], t['finishes']] for t in char_data.get('orderHallResearch', {}) if t['id'] > 0]
        # Don't overwrite with empty data
        if len(talents) == 0:
            return

        flat_talents = [item for sublist in talents for item in sublist] # why does this work? :|
        cd = CharacterData.objects.only('order_hall_talents').get(character=character)
        if cd.order_hall_talents != flat_talents:
            cd.order_hall_talents = flat_talents
            cd.save(update_fields=['order_hall_talents'])

        self.tt.add_time('loop order hall talents')

    # -----------------------------------------------------------------------

    def update_character_quests(self, character, char_data):
        quests = char_data.get('quests', {})
        valid_quests = list(sorted(q for q, c in quests.items() if c and q in VALID_QUESTS))
        cd = CharacterData.objects.only('quests_extra').get(character=character)
        if cd.quests_extra != valid_quests:
            cd.quests_extra = valid_quests
            cd.save(update_fields=['quests_extra'])

        self.tt.add_time('loop quests')

    # -----------------------------------------------------------------------

    def update_character_reputations(self, character, char_data):
        # Reputations have been scanned?
        reputations_scanned = self.scan_times.get('reputations')
        if not reputations_scanned:
            return

        # Reputations have been scanned since last time?
        reputations_scanned = self.utctimestamp(reputations_scanned)
        if character.last_reputation_update is not None and character.last_reputation_update >= reputations_scanned:
            return

        character.last_reputation_update = reputations_scanned
        self._update_fields.append('last_reputation_update')

        reputations = char_data.get('reputations', {})
        for factionID, rep_data in reputations.items():
            if factionID not in VALID_REPUTATIONS:
                continue

            # In-game level is 1 higher
            level = rep_data['level'] - 1
            # Fix the values too
            current = rep_data['current'] - _level_modifiers[level]
            max_value = rep_data['max_value'] - _level_modifiers[level]

            cr = character.reputation_map.get(factionID)
            if cr is None:
                self._new_reputations.append(CharacterReputation(
                    character=character,
                    faction_id=factionID,
                    level=level,
                    current=current,
                    max_value=max_value,
                ))
            else:
                updated_fields = update_fields(
                    cr,
                    dict(
                        level=level,
                        current=current,
                        max_value=max_value,
                    ),
                )
                if updated_fields:
                    cr.save(update_fields=updated_fields)

        paragons = char_data.get('paragons', {})
        for factionID, rep_data in paragons.items():
            if factionID not in VALID_PARAGONS:
                continue

            cr = character.reputation_map.get(factionID)
            if cr is not None:
                updated_fields = update_fields(
                    cr,
                    dict(
                        paragon_value=rep_data.get('value', 0),
                        paragon_max=rep_data.get('maxValue', 0),
                        paragon_ready=rep_data.get('hasReward', False),
                    ),
                )
                if updated_fields:
                    cr.save(update_fields=updated_fields)

        self.tt.add_time('loop reputations')

    # -----------------------------------------------------------------------

    def update_character_world_quests(self, character, char_data):
        # World Quests have been scanned?
        world_quests_scanned = self.scan_times.get('worldQuests')
        if not world_quests_scanned:
            print('%s WQs not scanned' % (character.id))
            return

        # World Quests have been scanned since last time?
        world_quests_scanned = self.utctimestamp(world_quests_scanned)
        if character.last_world_quest_update is not None and character.last_world_quest_update >= world_quests_scanned:
            print('%s WQs old' % (character.id))
            return

        character.last_world_quest_update = world_quests_scanned
        self._update_fields.append('last_world_quest_update')

        seen_expires = []

        world_quests = char_data.get('worldQuests', {})
        for day, wq_data in world_quests.items():
            expires_hour = round_to_nearest(wq_data['expires'], 3600)
            expires = self.utctimestamp(expires_hour)
            seen_expires.append(expires)

            # Global world quests
            if wq_data['faction'] > 0 and self.request.user.username in COOL_PEOPLE:
                index = int(day.split(' ')[1]) - 1
                self._global_world_quests[char_data['_region']]['factions'][index] = wq_data['faction']
                self._global_world_quests[char_data['_region']]['expires'][index] = expires_hour

            cwq = character.world_quest_map.get(expires)
            if cwq is None:
                self._new_world_quests.append(CharacterWorldQuest(
                    character=character,
                    faction_id=wq_data['faction'] if wq_data['faction'] > 0 else None,
                    expires=expires,
                    finished=wq_data['finished'],
                    completed_quests=wq_data['numCompleted'],
                    required_quests=wq_data['numRequired'],
                ))
            else:
                updated_fields = update_fields(
                    cwq,
                    dict(
                        finished=wq_data['finished'],
                        completed_quests=wq_data['numCompleted'],
                        required_quests=wq_data['numRequired'],
                    ),
                )
                if updated_fields:
                    cwq.save(update_fields=updated_fields)

        CharacterWorldQuest.objects.filter(character=character).exclude(expires__in=seen_expires).delete()

        self.tt.add_time('loop world quests')

    # -----------------------------------------------------------------------

    def update_character_followers(self, character, char_data):
        # Followers have been scanned?
        followers_scanned = self.scan_times.get('followers')
        if not followers_scanned:
            return

        # Followers have been scanned since last time?
        followers_scanned = self.utctimestamp(followers_scanned)
        if character.last_follower_update is not None and character.last_follower_update >= followers_scanned:
            return

        #character.last_follower_update = followers_scanned
        #self._update_fields.append('last_follower_update')

        valid_ids = set(GarrisonFollower.objects.values_list('pk', flat=True))

        seen_ids = []
        for follower_data in char_data.get('followers', []):
            if not isinstance(follower_data, dict):
                continue
            if follower_data['id'] not in valid_ids:
                continue

            seen_ids.append(follower_data['id'])

            cf = character.follower_map.get(follower_data['id'])
            if cf is None:
                self._new_followers.append(CharacterFollower(
                    character=character,
                    follower_id=follower_data['id'],
                    level=follower_data['level'],
                    quality=follower_data['quality'],
                    current_xp=follower_data['currentXP'],
                    level_xp=follower_data['levelXP'],
                    item_level=follower_data['itemLevel'],
                    vitality=int(follower_data.get('vitality', '0') or '0'),
                    max_vitality=int(follower_data.get('maxVitality', '0') or '0'),
                    is_troop=follower_data['isTroop'],
                    is_active=follower_data['isActive'],
                    abilities=follower_data['abilities'] or [],
                    equipment=follower_data['equipment'] or [],
                ))
            else:
                updated_fields = update_fields(
                    cf,
                    dict(
                        level=follower_data['level'],
                        quality=follower_data['quality'],
                        current_xp=follower_data['currentXP'],
                        level_xp=follower_data['levelXP'],
                        item_level=follower_data['itemLevel'],
                        vitality=int(follower_data.get('vitality', '0') or '0'),
                        max_vitality=int(follower_data.get('maxVitality', '0') or '0'),
                        is_troop=follower_data['isTroop'],
                        is_active=follower_data['isActive'],
                        abilities=follower_data['abilities'] or [],
                        equipment=follower_data['equipment'] or [],
                    ),
                )
                if updated_fields:
                    cf.save(update_fields=updated_fields)
    
        CharacterFollower.objects.filter(
            character=character,
        ).exclude(
            follower__in=seen_ids,
        ).delete()

        self.tt.add_time('loop followers')

    # -----------------------------------------------------------------------

    # def update_character_missions(self, character, char_data):
    #     # Missions have been scanned?
    #     missions_scanned = self.scan_times.get('missions')
    #     if not missions_scanned:
    #         return

    #     # Missions have been scanned since last time?
    #     missions_scanned = self.utctimestamp(missions_scanned)
    #     if character.last_mission_update is not None and character.last_mission_update >= missions_scanned:
    #         return

    #     character.last_mission_update = missions_scanned
    #     self._update_fields.append('last_mission_update')

    #     seen_ids = []
    #     for mission_data in char_data.get('missions', {}):
    #         if not isinstance(mission_data, dict):
    #             continue

    #         seen_ids.append(mission_data['id'])

    #         finishes = mission_data.get('finishes', None)
    #         if finishes:
    #             finishes = self.utctimestamp(finishes)
    #         followers = mission_data.get('followers', [])

    #         cm = character.mission_map.get(mission_data['id'])
    #         if cm is None:
    #             self._new_missions.append(CharacterMission(
    #                 character=character,
    #                 mission_id=mission_data['id'],
    #                 finishes=finishes,
    #                 followers=followers,
    #             ))
    #         elif cm.finishes != finishes or cm.followers != followers:
    #             cm.finishes = finishes
    #             update_fields = ['finishes']

    #             if (finishes and cm.followers and followers) or (finishes and not cm.followers and followers):
    #                 cm.followers = followers
    #                 update_fields.append('followers')
    #             elif not finishes:
    #                 cm.followers = []
    #                 update_fields.append('followers')

    #             cm.save(update_fields=update_fields)

    #     CharacterMission.objects.filter(
    #         character=character,
    #     ).exclude(
    #         mission__in=seen_ids,
    #     ).delete()

    #     self.tt.add_time('loop missions')

    # -----------------------------------------------------------------------

    def update_items(self):
        character_ids = set()
        item_ids = set()
        ci_map = {}
        item_map = {}

        ci_ids = set()

        for character, itemID, count, location, container, slot, quality, extra in self._all_items:
            character_ids.add(character.id)
            item_ids.add(itemID)

            item_map.setdefault(
                character.id,
                {},
            ).setdefault(
                location,
                {},
            ).setdefault(
                container,
                {},
            )

            item_map[character.id][location][container][slot] = (itemID, count, quality, extra)

            if character.id not in ci_map:
                ci_map[character.id] = {}
                #for ci in character.items.all():
                for ci in character._items:
                    ci_ids.add(ci.id)

                    ci_map[character.id].setdefault(
                        ci.location,
                        {},
                    ).setdefault(
                        ci.container,
                        {},
                    )[ci.slot] = ci

        # Add any new core.Item objects
        add_new_items(item_ids)

        # Now we can add/update all of that, ugh
        delete_q = []
        new_characteritems = []
        for characterID, locations in item_map.items():
            for location, containers in locations.items():
                for container, slots in containers.items():
                    delete_q.append(
                        Q(
                            character=characterID,
                            location=location,
                            container=container,
                        )
                        &
                        ~Q(slot__in=slots.keys())
                    )

                    #for itemID, count in items.items():
                    for slot, (itemID, count, quality, extra) in slots.items():
                        extra_json = json.dumps(extra)

                        ci = ci_map[characterID].get(location, {}).get(container, {}).get(slot, None)
                        if ci is None:
                            new_characteritems.append(CharacterItem(
                                character_id=characterID,
                                location=location,
                                container=container,
                                slot=slot,
                                item_id=itemID,
                                count=count,
                                quality=quality,
                                extra=extra_json,
                            ))
                        else:
                            ci_ids.remove(ci.id)

                            updated_fields = update_fields(
                                ci,
                                dict(
                                    item_id=itemID,
                                    count=count,
                                    slot=slot,
                                    quality=quality,
                                    extra=extra_json,
                                ),
                            )
                            if updated_fields:
                                ci.save(update_fields=updated_fields)

        # Create new objects
        CharacterItem.objects.bulk_create(new_characteritems)

        # Delete missing objects
        if ci_ids:
            CharacterItem.objects.filter(pk__in=ci_ids).delete()

    # -----------------------------------------------------------------------

    def get_weekly_reset(self, realm):
        reset_day = manual_data.weekly_resets.get(realm.region, 1)

        tz = pytz.timezone(realm.timezone)
        realm_now = timezone.now().astimezone(tz)
        weekday = realm_now.weekday()

        # It's past the right time
        if weekday > reset_day or (weekday == reset_day and realm_now.hour >= 5):
            days = 7 - weekday + reset_day
        else:
            days = reset_day - weekday

        reset_time = realm_now + datetime.timedelta(days=days)
        expires = tz.localize(datetime.datetime(reset_time.year, reset_time.month, reset_time.day, 5, 0, 0))

        return expires

# ---------------------------------------------------------------------------

class UploadHandlerAPI(UploadHandler):
    def response(Self, status, message):
        if status == messages.ERROR:
            return HttpResponseBadRequest("Invalid Lua data file.")
        else:
            return HttpResponse(message)

# ---------------------------------------------------------------------------
