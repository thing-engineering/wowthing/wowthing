import json
import re
import requests
import sys

from django.core.management.base import BaseCommand, CommandError

import data.manual as manual_data
from core.models import *
from core.util import add_new_items, update_fields

# ---------------------------------------------------------------------------

WOWHEAD_BASE = 'www'

BAGS_URL = 'http://%s.wowhead.com/items=1' % WOWHEAD_BASE
CURRENCIES_URL = 'http://%s.wowhead.com/currencies' % 'www' # PTR currency icons are fubar?
FACTIONS_URL = 'http://%s.wowhead.com/factions' % WOWHEAD_BASE
INSTANCES_URL = 'http://%s.wowhead.com/zones' % WOWHEAD_BASE
ORDER_HALL_TALENTS_URL = 'http://%s.wowhead.com/order-advancements' % WOWHEAD_BASE
PETS_URL = 'http://%s.wowhead.com/petspecies?filter=cr=9;crs=2;crv=0' % WOWHEAD_BASE
PROFESSIONS_URL = 'http://%s.wowhead.com/skills' % WOWHEAD_BASE
TOYS_URL = 'http://%s.wowhead.com/items?filter=cr=216;crs=1;crv=0' % WOWHEAD_BASE

BAGS_RE = re.compile(r'var listviewitems = (.*?);', re.S)
DATA_RE = re.compile(r'data: (.*?)\}\);', re.S)
TOY_ID_RE = re.compile(r'_\[(\d+)\]=')
TOYS_RE = re.compile(r'var _ = {};(.*?)\$.extend\(', re.S)
ZONEDATA_RE = re.compile(r'zonedata.zones = (.*?);', re.S)

WOW_STRIP_RE = re.compile(r'\|c[0-9A-F]{8,}(.*?)\|r')

# ---------------------------------------------------------------------------

INSTANCE_EXTRAS = (
    dict(id=100001, instance=1, name='Sha of Anger', expansion=4),
    dict(id=100002, instance=1, name='Galleon', expansion=4),
    dict(id=100003, instance=1, name='Nalak', expansion=4),
    dict(id=100004, instance=1, name='Oondasta', expansion=4),
    dict(id=100005, instance=1, name='The Four Celestials', expansion=4),
    dict(id=100006, instance=1, name='Ordos', expansion=4),
    dict(id=100101, instance=1, name='Gorgrond Bosses', expansion=5),
    dict(id=100102, instance=1, name='Rukhmar', expansion=5),
    dict(id=100103, instance=1, name='Supreme Lord Kazzak', expansion=5),
    dict(id=100190, instance=1, name='Garrison Invasion', expansion=5),
    dict(id=100201, instance=1, name='Legion Bosses', expansion=6),
    dict(id=100202, instance=1, name='Greater Invasions', expansion=6),
    dict(id=100203, instance=1, name='Pristine Argunite', expansion=6),
)

INSTANCE_RENAMES = {
    "Caverns of Time: Hyjal Summit": 'The Battle for Mount Hyjal',
    'Coilfang Reservoir: Serpentshrine Cavern': 'Coilfang: Serpentshrine Cavern',
    'Coilfang Reservoir: The Slave Pens': 'Coilfang: The Slave Pens',
    'Coilfang Reservoir: The Steamvault': 'Coilfang: The Steamvault',
    'Coilfang Reservoir: The Underbog': 'Coilfang: The Underbog',
    "Crusaders' Coliseum: Trial of the Champion": 'Trial of the Champion',
    "Crusaders' Coliseum: Trial of the Crusader": 'Trial of the Crusader',
    'Hellfire Citadel: Hellfire Ramparts': 'Hellfire Citadel: Ramparts',
    "Hellfire Citadel: Magtheridon's Lair": "Magtheridon's Lair",
    'Icecrown Citadel: Halls of Reflection': 'Halls of Reflection',
    'Icecrown Citadel: Pit of Saron': 'Pit of Saron',
    'Icecrown Citadel: The Forge of Souls': 'The Forge of Souls',
    "Magisters' Terrace": "Magister's Terrace",
    'Sunwell Plateau': 'The Sunwell',
    'Tempest Keep: The Eye': 'Tempest Keep',
    "Temple of Ahn'Qiraj": "Ahn'Qiraj Temple",
    'The Deadmines': 'Deadmines',
    'The Nexus: The Eye of Eternity': 'The Eye of Eternity',
    'The Seat of the Triumvirate': 'Seat of the Triumvirate',
    'Utgarde Keep: Utgarde Keep': 'Utgarde Keep',
    'Utgarde Keep: Utgarde Pinnacle': 'Utgarde Pinnacle',
    'Violet Hold': 'Assault on Violet Hold',
    'Wyrmrest Temple: The Obsidian Sanctum': 'The Obsidian Sanctum',
    'Wyrmrest Temple: The Ruby Sanctum': 'The Ruby Sanctum',
}

INSTANCE_SHORT = {
    # Battle for Azeroth
    "Atal'Dazar": 'AD',
    'Freehold': 'FH',
    'Shrine of the Storm': 'SotS',
    'Siege of Boralus': 'SoB',
    'Temple of Sethraliss': 'ToS',
    'The MOTHERLODE!!': 'TM!',
    'The Underrot': 'TU',
    'Uldir': 'Uld',

    # Legion
    'The Nighthold': 'NH',

    # WoD
    'Blackrock Foundry': 'BRF',
    'Grimrail Depot': 'GRD',
    'Hellfire Citadel': 'HFC',
    'Highmaul': 'HM',
    'Upper Blackrock Spire': 'UBRS',

    'Garrison Invasion': 'Inv',
    'Gorgrond Bosses': 'Gorg',
    'Rukhmar': 'Ruk',

    # MoP
    "Mogu'shan Vaults": 'MSV',
    'Scholomance': 'Sch',

    # Cata
    'Firelands': 'FL',
    "Zul'Aman": 'ZA',
    "Zul'Gurub": 'ZG',

    # WotLK
    "Trial of the Champion": 'ToC5',
    "Trial of the Crusader": 'ToC',
    'Icecrown Citadel': 'ICC',
    'Naxxramas': 'Naxx',
    "Onyxia's Lair": 'Ony',
    'Ulduar': 'Uld',

    # TBC
    'Coilfang: Serpentshrine Cavern': 'SSC',
    'Coilfang: The Slave Pens': 'SP',
    'Coilfang: The Steamvault': 'SV',
    'Coilfang: The Underbog': 'UB',
    'Karazhan': 'Kara',
    'The Battle for Mount Hyjal': 'Hyjal',

    # Classic
    "Ahn'Qiraj Temple": 'AQ40',
    'Blackwing Descent': 'BWD',
    'Blackwing Lair': 'BWL',
    "Ruins of Ahn'Qiraj": 'AQ20',
    'The Sunwell': 'SWP',

    # World bosses
    'Galleon': 'Gal',
    'Nalak': 'Nalak',
    'Oondasta': 'Oon',
    'Ordos': 'Ordos',
    'Sha of Anger': 'Sha',
    'The Four Celestials': '4C',

    # ???
    'Caverns of Time: Old Hillsbrad Foothills': 'OHF',
    'Caverns of Time: The Culling of Stratholme': 'CoS',
}

INSTANCE_EXPANSION = {
    'The MOTHERLODE!!': 7,
}

PET_SKIP = [
    56082, 56083, # Balloons are not pets
    79730, # River Calf
    124633, # M-37 (NPC pet)
    124634, # Corporal Hammer (NPC pet)
]

# ---------------------------------------------------------------------------

class Command(BaseCommand):
    def handle(self, *args, **options):
        self._new_items = set()

        #self.update_bags()
        self.update_factions()
        self.update_instances()
        self.update_order_hall_talents()
        self.update_pets()
        self.update_professions()
        self.update_toys()

        add_new_items(self._new_items)

    # -----------------------------------------------------------------------

    def update_bags(self):
        print 'Fetching bags...',
        sys.stdout.flush()

        r = requests.get(BAGS_URL)
        if r.status_code == requests.codes.ok:
            m = BAGS_RE.search(r.text)
            if m:
                print 'done.'

                sigh = m.group(1).replace(',firstseenpatch:', ',"firstseenpatch":').replace(',frommerge:', ',"frommerge":')
                #open('/tmp/sigh.txt', 'w').write(sigh)
                bags = json.loads(sigh)

                bag_map = {}
                for item in Item.objects.filter(slot=18):
                    bag_map[item.id] = item

                f = open('bag_slot.data', 'w')

                new = []
                for data in bags:
                    bag = bag_map.get(int(data['id']))
                    if bag is None:
                        new.append(Item(
                            id=data['id'],
                            name='###UPDATE###',
                        ))
                    f.write('%d=%d\n' % (data['id'], data['nslots']))

                f.close()

                Item.objects.bulk_create(new)

            else:
                print 'fail (re search).'
        else:
            print 'fail (%s).' % (r.status_code)


    # -----------------------------------------------------------------------

    def update_currencies(self):
        print 'Fetching currencies...',
        sys.stdout.flush()

        r = requests.get(CURRENCIES_URL)
        if r.status_code == requests.codes.ok:
            m = DATA_RE.search(r.text)
            if m:
                print 'done.'

                currencies = json.loads(m.group(1))

                currency_map = {}
                for currency in Currency.objects.all():
                    currency_map[currency.id] = currency

                new = []
                seen_ids = []
                for data in currencies:
                    if data['name'] in ('n/a', 'UNUSED'):
                        continue

                    currency = currency_map.get(int(data['id']))
                    if currency is None:
                        new.append(Currency(
                            id=data['id'],
                            name=data['name'],
                            category=data['category'],
                            icon=data['icon'],
                        ))
                    elif currency.name != data['name'] or currency.category != data['category'] or \
                        currency.icon != data['icon']:

                        currency.name = data['name']
                        currency.category = data['category']
                        currency.icon = data['icon']
                        currency.save()

                    seen_ids.append(data['id'])

                Currency.objects.bulk_create(new)
                Currency.objects.exclude(pk__in=seen_ids).delete()

            else:
                print 'fail (re search).'
        else:
            print 'fail (%s).' % (r.status_code)

    # -----------------------------------------------------------------------

    def update_factions(self):
        print 'Fetching factions...',
        sys.stdout.flush()

        r = requests.get(FACTIONS_URL)
        if r.status_code == requests.codes.ok:
            m = DATA_RE.search(r.text)
            if m:
                print 'done.'

                factions = json.loads(m.group(1))

                faction_map = {}
                for faction in Faction.objects.all():
                    faction_map[faction.id] = faction

                new = []
                for data in factions:
                    category = data.get('category', 0)
                    category2 = data.get('category2', 0)
                    expansion = data.get('expansion', 0)
                    side = data.get('side', 0)

                    faction = faction_map.get(int(data['id']))
                    if faction is None:
                        new.append(Faction(
                            id=data['id'],
                            name=data['name'],
                            category=category,
                            category2=category2,
                            expansion=expansion,
                            side=side,
                        ))
                    elif faction.name != data['name'] or faction.category != category or faction.category2 != category2 or \
                        faction.expansion != expansion or faction.side != side:
                        faction.name = data['name']
                        faction.category = category
                        faction.category2 = category2
                        faction.expansion = expansion
                        faction.side = faction.side
                        faction.save()

                Faction.objects.bulk_create(new)

            else:
                print 'fail (re search).'
        else:
            print 'fail (%s).' % (r.status_code)

    # -----------------------------------------------------------------------

    def update_instances(self):
        print 'Fetching instances...',
        sys.stdout.flush()

        r = requests.get(INSTANCES_URL)
        if r.status_code == requests.codes.ok:
            m = ZONEDATA_RE.search(r.text)
            if m:
                print 'done.'

                instances = json.loads(m.group(1))
                instances.extend(INSTANCE_EXTRAS)

                instance_map = {}
                for instance in Instance.objects.all():
                    instance_map[instance.id] = instance

                expansion_map = {}
                new = []
                for data in instances:
                    if data.get('instance') is None:
                        continue

                    cat = data.get('category')
                    if cat == -1:
                        continue

                    #if ':' in data['name']:
                    #    data['name'] = data['name'].split(': ', 1)[1]

                    if '(' in data['name']:
                        data['name'] = data['name'].split(' (', 1)[0]

                    data['name'] = INSTANCE_RENAMES.get(data['name'], data['name'])

                    short_name = INSTANCE_SHORT.get(data['name'])
                    if short_name is None:
                        short_name = ''.join(w[0] for w in data['name'].split() if w.lower() != 'the')

                    if len(short_name) > 5:
                        print 'Short name too long: %r %r' % (data['name'], short_name)
                        short_name = ''

                    expansion = expansion_map.get(data['name'], INSTANCE_EXPANSION.get(data['name'], data.get('expansion', 0)))
                    expansion_map[data['name']] = expansion

                    instance = instance_map.get(int(data['id']))
                    if instance is None:
                        new.append(Instance(
                            id=data['id'],
                            name=data['name'],
                            short_name=short_name,
                            expansion=expansion,
                        ))
                    elif instance.name != data['name'] or instance.short_name != short_name or \
                        instance.expansion != expansion:

                        instance.name = data['name']
                        instance.short_name = short_name
                        instance.expansion = expansion
                        instance.save()

                Instance.objects.bulk_create(new)

            else:
                print 'fail (re search).'
        else:
            print 'fail (%s).' % (r.status_code)

    # -----------------------------------------------------------------------

    def update_order_hall_talents(self):
        print 'Fetching order hall talents...',
        sys.stdout.flush()

        r = requests.get(ORDER_HALL_TALENTS_URL)
        if r.status_code == requests.codes.ok:
            m = DATA_RE.search(r.text)
            if m:
                json_data = m.group(1).replace('undefined', 'null').replace("\\'", "'").strip()
                talents = json.loads(json_data)

                print '%d talents.' % (len(talents))

                talent_map = {}
                for talent in GarrisonTalent.objects.all():
                    talent_map[talent.id] = talent

                mask_map = {}
                for i in range(1, 13):
                    mask_map[1 << (i - 1)] = i

                new = []
                for talent_data in talents:
                    classmask = int(talent_data['classmask'])
                    if classmask == 0:
                        continue
                    class_id = mask_map[int(talent_data['classmask'])]

                    talent_data['icon'] = talent_data['icon'].replace('-', '_')

                    talent = talent_map.get(int(talent_data['id']))
                    if talent is None:
                        new.append(GarrisonTalent(
                            id=talent_data['id'],
                            class_id=class_id,
                            tier=talent_data['tier'],
                            column=talent_data['column'],
                            name=talent_data['name'],
                            description=talent_data['description'],
                            icon=talent_data['icon'],
                        ))
                    elif talent.name != talent_data['name'] or talent.icon != talent_data['icon']:
                        talent.name = talent_data['name']
                        talent.icon = talent_data['icon']
                        talent.save()

                GarrisonTalent.objects.bulk_create(new)

            else:
                print 'fail (re search).'
        else:
            print 'fail (%s).' % (r.status_code)

    # -----------------------------------------------------------------------

    def update_pets(self):
        print 'Fetching pets...',
        sys.stdout.flush()

        r = requests.get(PETS_URL)
        if r.status_code == requests.codes.ok:
            m = DATA_RE.search(r.text)
            if m:
                json_data = m.group(1).replace('undefined', 'null').replace("\\'", "'").strip()
                if json_data[-1] == ',':
                    json_data = json_data[:-1]
                pets = json.loads(json_data)

                print '%d pets.' % (len(pets))

                pet_map = {}
                for pet in BattlePet.objects.all():
                    pet_map[pet.id] = pet

                new = []
                for pet_data in pets:
                    pet_data['icon'] = pet_data['icon'].replace('-', '_')

                    pet = pet_map.get(int(pet_data['npc']['id']))
                    if pet is None:
                        new.append(BattlePet(
                            id=pet_data['npc']['id'],
                            name=pet_data['name'],
                            icon=pet_data['icon'],
                            family=pet_data['type'],
                            species_id=pet_data['species'],
                        ))
                    else:
                        updated_fields = update_fields(
                            pet,
                            dict(
                                name=pet_data['name'],
                                icon=pet_data['icon'],
                                family=pet_data['type'],
                                species_id=pet_data['species'],
                            ),
                        )
                        if updated_fields:
                            pet.save(update_fields=updated_fields)

                BattlePet.objects.bulk_create(new)

                diff = set(pet_map.keys()).difference(data.all_pets).difference(PET_SKIP)
                if diff:
                    print 'Missing pets:', sorted(diff)

            else:
                print 'fail (re search).'
        else:
            print 'fail (%s).' % (r.status_code)

    # -----------------------------------------------------------------------

    def update_professions(self):
        print 'Fetching professions...',
        sys.stdout.flush()

        r = requests.get(PROFESSIONS_URL)
        if r.status_code == requests.codes.ok:
            m = DATA_RE.search(r.text)
            if m:
                print 'done.'

                json_data = m.group(1).replace("icon:'", '"icon":"').replace("'}", '"}')
                professions = json.loads(json_data)

                profession_map = {}
                for profession in Profession.objects.all():
                    profession_map[profession.id] = profession

                new = []
                for data in professions:
                    profession = profession_map.get(int(data['id']))
                    if profession is None:
                        new.append(Profession(
                            id=data['id'],
                            name=data['name'],
                            icon=data['icon'],
                        ))
                    elif profession.name != data['name'] or profession.icon != data['icon']:
                        profession.name = data['name']
                        profession.icon = data['icon']
                        profession.save()

                Profession.objects.bulk_create(new)

            else:
                print 'fail (re search).'
        else:
            print 'fail (%s).' % (r.status_code)

    # -----------------------------------------------------------------------

    def update_toys(self):
        print 'Fetching toys...',
        sys.stdout.flush()

        r = requests.get(TOYS_URL)
        if r.status_code == requests.codes.ok:
            m = TOYS_RE.search(r.text)
            if m:
                print 'done.'

                toy_ids = set()
                for chunk in m.group(1).split(';'):
                    m = TOY_ID_RE.search(chunk)
                    if m:
                        toy_ids.add(int(m.group(1)))

                existing = set(Item.objects.filter(pk__in=toy_ids).values_list('pk', flat=True))

                new = []
                for toy_id in toy_ids.difference(existing):
                    new.append(Item(
                        id=toy_id,
                        name='###UPDATE###',
                    ))

                Item.objects.bulk_create(new)

                diff = toy_ids.difference(data.all_toys)
                if diff:
                    print 'Missing toys:', diff

            else:
                print 'fail (re search).'
        else:
            print 'fail (%s).' % (r.status_code)

# ---------------------------------------------------------------------------
