import csv
import json

from django.core.management.base import BaseCommand, CommandError

# ---------------------------------------------------------------------------

class Command(BaseCommand):
    def handle(self, *args, **options):
        achievement_map = {}
        criteriatree_map = {}
        criteriatree_children = {}
        criteria_map = {}

        # Id,Supersedes,CriteriaTreeId
        with open('/home/freddie/achievement.csv', 'rb') as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                try:
                    achievementId = int(row[0])
                    requiredAchievementId = int(row[1])
                    criteriaTreeId = int(row[2])
                    minimumCriteria = int(row[3])

                    if criteriaTreeId < 0:
                        criteriaTreeId = criteriaTreeId + 65536
                    
                    achievement_map[achievementId] = dict(
                        requiredAchievementId=requiredAchievementId,
                        criteriaTreeId=criteriaTreeId,
                        minimumCriteria=minimumCriteria,
                    )
                
                except:
                    pass

        #Id,Amount,Name,ParentId,CriteriaId,Order,Flags
        with open('/home/freddie/criteriatree.csv', 'rb') as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                try:
                    criteriaTreeId = int(row[0])
                    amount = int(row[1])
                    description = row[2]
                    parentId = int(row[3])
                    criteriaId = int(row[4])
                    order = int(row[5])
                    flags = int(row[6])

                    if parentId < 0:
                        parentId = parentId + 65536
                    if criteriaId < 0:
                        criteriaId = criteriaId + 65536

                    if parentId not in criteriatree_children:
                        criteriatree_children[parentId] = []
                    criteriatree_children[parentId].append(criteriaTreeId)

                    criteriatree_map[criteriaTreeId] = dict(
                        id=criteriaTreeId,
                        amount=amount,
                        description=description,
                        parentId=parentId,
                        criteriaId=criteriaId,
                        order=order,
                        flags=flags,
                    )

                except Exception, e:
                    pass

        #id,value,type
        #72,0,10
        with open('/home/freddie/criteria.csv', 'rb') as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                try:
                    criteriaId = int(row[0])
                    value = int(row[1])
                    typ = int(row[2])

                    criteria_map[criteriaId] = dict(
                        id=criteriaId,
                        value=value,
                        type=typ,
                    )

                except:
                    pass


        data = dict(achievements={}, chains={}, criteria={})

        for achievementId, achievement_data in achievement_map.items():
            requiredAchievementId = achievement_data['requiredAchievementId']
            rootCriteriaTreeId = achievement_data['criteriaTreeId']
            minimumCriteria = achievement_data['minimumCriteria']
            if rootCriteriaTreeId == 0:
                continue

            if rootCriteriaTreeId not in criteriatree_map:
                print achievementId, rootCriteriaTreeId, 'not found'
                continue

            if requiredAchievementId > 0:
                data['chains'][requiredAchievementId] = achievementId


            rootCriteriaTree = criteriatree_map[rootCriteriaTreeId]
            criteriaTrees = [criteriatree_map[i] for i in criteriatree_children.get(rootCriteriaTree['id'], [])]
            if len(criteriaTrees) == 0:
                criteriaTrees = [rootCriteriaTree]

            argh = []
            for ct in criteriaTrees:
                if ct['id'] in criteriatree_children:
                    argh.extend(criteriatree_map[i] for i in criteriatree_children[ct['id']])
                else:
                    argh.append(ct)

            criteriaTrees = argh
            criteriaTrees.sort(key=lambda x: x['order'])

            ach_achievement = [minimumCriteria]
            ach_criteria = []
            for criteriaTree in criteriaTrees:
                ach_achievement.append([criteriaTree['criteriaId'], criteriaTree['amount']])

                #{"assetID": 0, "flags": 1, "criteriaID": 19598, "type": 156, "description": "Obtain 15 unique companion pets"}
                if criteriaTree['criteriaId'] > 0 and criteriaTree['criteriaId'] in criteria_map:
                    criteria = criteria_map[criteriaTree['criteriaId']]

                    description = criteriaTree['description']
                    if description.strip() == '':
                        description = criteriatree_map[criteriaTree['parentId']]['description']

                    ach_criteria.append(dict(
                        assetID=criteria['value'],
                        flags=criteriaTree['flags'],
                        criteriaID=criteria['id'],
                        type=criteria['type'],
                        description=description,
                    ))

                
            if achievementId == 5211:
                print ach_criteria

            data['achievements'][achievementId] = ach_achievement
            data['criteria'][achievementId] = ach_criteria

        open('achievement_data.json', 'w').write(json.dumps(data))

        # luadata = open('/home/freddie/!DumpData.lua').read().replace(
        #     'DumpDataSaved = {',
        #     '{',
        # ).replace(
        #     '["chains"] = {',
        #     '["chains"] = { nil,',
        # )

        # data = slpp.decode(luadata)

        # for k, v in data['chains'].items():
        #     if v is None:
        #         del data['chains'][k]

        # open('achievement_data.json', 'w').write(json.dumps(data))

# ---------------------------------------------------------------------------
