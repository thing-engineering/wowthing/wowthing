from django.db import models

# ---------------------------------------------------------------------------

class Realm(models.Model):
    region = models.CharField(max_length=2)
    name = models.CharField(max_length=30)
    long_name = models.CharField(max_length=200)
    slug = models.CharField(max_length=30)
    locale = models.CharField(max_length=5)
    timezone = models.CharField(max_length=30)

    first_realm = models.BooleanField(default=False)
    last_api_update = models.DateTimeField(blank=True, null=True)

    def get_name(self):
        return '%s (%s)' % (self.name, self.region.upper())

    def as_dict(self):
        return dict(
            id=self.id,
            region=self.region,
            name=self.name,
            slug=self.slug,
            long_name=self.long_name,
        )

# ---------------------------------------------------------------------------
