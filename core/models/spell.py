from django.db import models

# ---------------------------------------------------------------------------

class Spell(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=64)
    icon = models.CharField(max_length=128, default='')

# ---------------------------------------------------------------------------
