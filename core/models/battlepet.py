from django.db import models

# ---------------------------------------------------------------------------

class BattlePet(models.Model):
    HUMANOID_FAMILY = 1
    DRAGONKIN_FAMILY = 2
    FLYING_FAMILY = 3
    UNDEAD_FAMILY = 4
    CRITTER_FAMILY = 5
    MAGIC_FAMILY = 6
    ELEMENTAL_FAMILY = 7
    BEAST_FAMILY = 8
    AQUATIC_FAMILY = 9
    MECHANICAL_FAMILY = 10

    FAMILY_CHOICES = (
        (HUMANOID_FAMILY, 'Humanoid'),
        (DRAGONKIN_FAMILY, 'Dragonkin'),
        (FLYING_FAMILY, 'Flying'),
        (UNDEAD_FAMILY, 'Undead'),
        (CRITTER_FAMILY, 'Critter'),
        (MAGIC_FAMILY, 'Magic'),
        (ELEMENTAL_FAMILY, 'Elemental'),
        (BEAST_FAMILY, 'Beast'),
        (AQUATIC_FAMILY, 'Aquatic'),
        (MECHANICAL_FAMILY, 'Mechanical'),
    )

    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=64)
    icon = models.CharField(max_length=128)
    species_id = models.IntegerField(db_index=True)
    family = models.SmallIntegerField(choices=FAMILY_CHOICES)

# ---------------------------------------------------------------------------
