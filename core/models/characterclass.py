from django.db import models

# ---------------------------------------------------------------------------

class CharacterClass(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=16)
    icon = models.CharField(max_length=40)

    def as_dict(self):
        return dict(
            id=self.id,
            name=self.name,
            icon=self.icon,
        )

# ---------------------------------------------------------------------------
