from django.db import models

# ---------------------------------------------------------------------------

class Battlegroup(models.Model):
    name = models.CharField(max_length=16)
    region = models.CharField(max_length=2)

# ---------------------------------------------------------------------------
