from django.db import models

# ---------------------------------------------------------------------------

class GarrisonTalent(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    class_id = models.SmallIntegerField()
    tier = models.SmallIntegerField()
    column = models.SmallIntegerField()
    name = models.CharField(max_length=64, default='')
    description = models.TextField(default='')
    icon = models.CharField(max_length=128, default='')

# ---------------------------------------------------------------------------
"""
{  
  "id":405,
  "icon":"ability_warrior_rallyingcry",
  "name":"Abundant Valor",
  "description":"Increases the maximum number of Valarjar Aspirants, Stormforged Valarjar, Shieldmaiden Warbands and Valkyra Shieldmaidens you can recruit by 1.",
  "tier":2,
  "column":0,
  "researchtime":21600,
  "researchcost":500,
  "researchcurrency":1220,
  "switchtime":86400,
  "switchcost":2000,
  "switchcurrency":1220,
  "spell":0,
  "classmask":1,
  "requirement":"You need to be level 110 to research."
}
"""
