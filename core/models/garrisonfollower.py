from django.contrib.postgres.fields import ArrayField
from django.db import models

# ---------------------------------------------------------------------------

class GarrisonFollower(models.Model):
    id = models.SmallIntegerField(primary_key=True)

    alliance_name = models.CharField(max_length=32)
    alliance_portrait = models.CharField(max_length=32)
    horde_name = models.CharField(max_length=32)
    horde_portrait = models.CharField(max_length=32)

# ---------------------------------------------------------------------------

# {
# "id":216,
# "npcalliance":89075,
# "npcalliancemodel":59353,
# "npchorde":89133,
# "npchordemodel":56610,
# "racealliance":5,
# "racehorde":13,
# "quality":3,
# "level":91,
# "itemlevelarmor":600,
# "itemlevelweapon":600,
# "namealliance":"Delvar Ironfist",
# "namehorde":"Vivianne",
# "name":"Delvar Ironfist \/ Vivianne",
# "classalliance":"Blood Death Knight",
# "classhorde":"Fire Mage",
# "baseclassalliance":6,
# "baseclasshorde":8,
# "portraitalliance":"followerportrait_59353",
# "portraithorde":"followerportrait_56610",
# "sourcehorde":"<span style=\"color: #FFD200\">Quest:<\/span> Establish Your Garrison<br \/><span style=\"color: #FFD200\">Zone:<\/span> Frostfire Ridge",
# "sourcealliance":"<span style=\"color: #FFD200\">Quest:<\/span> A Surly Dwarf<br \/><span style=\"color: #FFD200\">Zone:<\/span> Ashran",
# "itemsethorde":91,
# "itemsetalliance":41,
# "abilities":{
#     "2":[
#         "5",
#         "231"
#     ],
#     "1":[
#         "115",
#         "231"
#     ]
# }},
