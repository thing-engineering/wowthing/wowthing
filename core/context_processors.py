from django.conf import settings
from django.core.cache import cache
from django.db.models import Q

from teams.models import Team

# ---------------------------------------------------------------------------

def wowthing(request):
    teams = []
    if request.user.is_authenticated:
        cache_key = 'teams:%d' % (request.user.id)
        teams = cache.get(cache_key, None)
        if teams is None:
            teams = list(Team.objects.filter(
                Q(owner=request.user)
                |
                Q(members__character__bnetaccount__user=request.user)
            ).distinct())

            cache.set(cache_key, teams, 60)

    return dict(
        teams=teams,
        wowthing_version="¯\_(ツ)_/¯",
    )

# ---------------------------------------------------------------------------
