# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-02-18 07:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0009_auto_20180131_2245'),
    ]

    operations = [
        migrations.AlterField(
            model_name='garrisonability',
            name='icon',
            field=models.CharField(default=b'', max_length=128),
        ),
    ]
