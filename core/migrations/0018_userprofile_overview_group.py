# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-05-05 05:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0017_userprofile_overview_show_totals'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='overview_group',
            field=models.CharField(default=b'default', max_length=128),
        ),
    ]
