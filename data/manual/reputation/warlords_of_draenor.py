reputations_warlords = dict(
    name='Warlords of Draenor',
    key='wod',
    reputations=[
        # Tanaan Jungle
        [
            dict(
                alliance_id=1847,
                alliance_name='Hand of the Prophet',
                alliance_icon='inv_guild_standard_alliance_c',
                horde_id=1848,
                horde_name="Vol'jin's Headhunters",
                horde_icon='inv_guild_standard_horde_c',
            ),
            dict(id=1849, name='Order of the Awakened', icon='inv_tabard_a_82awakenedorder'),
            dict(id=1850, name='The Saberstalkers', icon='inv_misc_blacksaberonfang'),
        ],
        # Other
        [
            dict(
                alliance_id=1731,
                alliance_name='Council of Exarchs',
                alliance_icon='inv_tabard_a_81exarchs',
                horde_id=1445,
                horde_name='Frostwolf Orcs',
                horde_icon='inv_tabard_a_01frostwolfclan',
            ),
            dict(
                alliance_id=1710,
                alliance_name="Sha'tari Defense",
                alliance_icon='inv_tabard_a_shataridefense',
                horde_id=1708,
                horde_name='Laughing Skull Orcs',
                horde_icon='inv_tabard_a_80laughingskull',
            ),
            dict(
                alliance_id=1682,
                alliance_name="Wrynn's Vanguard",
                alliance_icon='inv_tabard_a_78wrynnvanguard',
                horde_id=1681,
                horde_name="Vol'jin's Spear",
                horde_icon='inv_tabard_a_77voljinsspear',
            ),
            dict(id=1515, name='Arakkoa Outcasts', icon='inv_tabard_a_76arakkoaoutcast'),
            dict(id=1711, name='Steamwheedle Preservation Society', icon='achievement_goblinhead'),
        ],
        # Friends (bodyguards)
        [
            dict( # 90 Ashran
                alliance_id=1733,
                alliance_name='Delvar Ironfist',
                alliance_icon='FollowerPortrait_59353',
                horde_id=1739,
                horde_name='Vivianne',
                horde_icon='FollowerPortrait_56610',
                bodyguard=True,
            ),
            dict(id=1736, name='Tormmok', icon='FollowerPortrait_57173', bodyguard=True), # 92 Gorgrond
            dict( # 94 Talador
                alliance_id=1738,
                alliance_name='Defender Illona',
                alliance_icon='FollowerPortrait_59839',
                horde_id=1740,
                horde_name='Aeda Brightdawn',
                horde_icon='FollowerPortrait_59874',
                bodyguard=True,
            ),
            dict(id=1741, name='Leorajh', icon='FollowerPortrait_61487', bodyguard=True),
            dict(id=1737, name='Talonpriest Ishaal', icon='FollowerPortrait_59461', bodyguard=True),
        ],
    ],
)
