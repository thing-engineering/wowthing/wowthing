from ._misc import *
from .battle_for_azeroth import *
from .legion import *
from .warlords_of_draenor import *
from .mists_of_pandaria import *
from .cataclysm import *
from .wrath_of_the_lich_king import *
from .the_burning_crusade import *
from .vanilla import *
from .pvp import *

# ---------------------------------------------------------------------------

reputations = [
    reputations_bfa,
    reputations_legion,
    reputations_warlords,
    reputations_pandaria,
    reputations_cataclysm,
    reputations_wrath,
    reputations_tbc,
    reputations_vanilla,
    reputations_pvp,
]

# ---------------------------------------------------------------------------
# Magic, don't touch
bodyguard_factions = []
for rep_group in reputations:
    for rep_set in rep_group['reputations']:
        for rep in rep_set:
            if rep.get('bodyguard', False) == True:
                if 'alliance_id' in rep:
                    bodyguard_factions.append(rep['alliance_id'])
                    bodyguard_factions.append(rep['horde_id'])
                else:
                    bodyguard_factions.append(rep['id'])

faction_icons = {}
for expansion in reputations:
    for rep_group in expansion['reputations']:
        for rep in rep_group:
            if 'alliance_id' in rep:
                faction_icons[rep['alliance_id']] = rep['alliance_icon']
                faction_icons[rep['horde_id']] = rep['horde_icon']
            else:
                faction_icons[rep['id']] = rep['icon']
