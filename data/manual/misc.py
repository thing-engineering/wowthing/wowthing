import json
import os

from core.models import Currency, Instance, Realm

# ---------------------------------------------------------------------------

def relpath(f):
    return os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), f))

# ---------------------------------------------------------------------------

wowdb_base = 'www.wowdb.com'
wowhead_base = 'www.wowhead.com'

# ---------------------------------------------------------------------------
# Achievements
achievements = json.load(open(relpath('../raw/achievement.json')))['achievements']
criteria_map = json.load(open(relpath('../raw/criteria_map.json')))

# ---------------------------------------------------------------------------
# Expansions
expansions = {
    7: dict(
        name='Battle for Azeroth',
        short_name='BfA',
        max_level=120,
    ),
    6: dict(
        name='Legion',
        short_name='Leg',
        max_level=110,
    ),
    5: dict(
        name='Warlords of Draenor',
        short_name='WoD',
        max_level=100,
    ),
    4: dict(
        name='Mists of Pandaria',
        short_name='MoP',
        max_level=90,
    ),
    3: dict(
        name='Cataclysm',
        short_name='Cata',
        max_level=85,
    ),
    2: dict(
        name='Wrath of the Lich King',
        short_name='WotLK',
        max_level=80,
    ),
    1: dict(
        name='The Burning Crusade',
        short_name='TBC',
        max_level=70,
    ),
    0: dict(
        name='World of Warcraft',
        short_name='WoW',
        max_level=60,
    ),
}

# ---------------------------------------------------------------------------
# Used as the base for item level coloration. +15 = LFR, +30 = Normal, etc
base_item_level = 385

# Current expansion instead of hardcoding it
current_expansion = max(k for k in expansions.keys())

# Maximum player level instead of hardcoding it
max_player_level = max(v['max_level'] for v in expansions.values())

# ---------------------------------------------------------------------------
# Weekly quest resets
weekly_resets = {
    # US realms (including Oceanic) reset Tuesday
    'us': 1,
    # EU realms reset Wednesday
    'eu': 2,
}

region_armory = {
    'us': 'en-us',
    'eu': 'en-gb',
    'kr': 'ko-kr',
}

# ---------------------------------------------------------------------------
# Profession cooldowns
profession_cooldowns = {
    # # Alchemy
    # 171: [
    #     dict(
    #         id=156587,
    #         name='Alchemical Catalyst',
    #         icon='inv_enchant_alchemycatalyst',
    #     ),
    #     dict(
    #         id=175880,
    #         name='Secrets of Draenor Alchemy',
    #         icon='inv_misc_book_08',
    #     ),
    # ],

    # # Blacksmithing
    # 164: [
    #     dict(
    #         id=171690,
    #         name='Truesteel Ingot',
    #         icon='inv_misc_trueironingot',
    #     ),
    #     dict(
    #         id=176090,
    #         name='Secrets of Draenor Blacksmithing',
    #         icon='inv_misc_book_11',
    #     ),
    # ],

    # # Enchanting
    # 333: [
    #     dict(
    #         id=169092,
    #         name='Temporal Crystal',
    #         icon='inv_enchanting_wod_crystalshard4',
    #     ),
    #     dict(
    #         id=177043,
    #         name='Secrets of Draenor Enchanting',
    #         icon='inv_misc_book_08',
    #     ),
    # ],
    # # Engineering
    # 202: [
    #     dict(
    #         id=169080,
    #         name='Gearspring Parts',
    #         icon='inv_eng_gearspringparts',
    #     ),
    #     dict(
    #         id=177054,
    #         name='Secrets of Draenor Engineering',
    #         icon='inv_misc_book_08',
    #     ),
    # ],
    # # Inscription
    # 773: [
    #     dict(
    #         id=169081,
    #         name='War Paints',
    #         icon='inv_inscription_warpaint_blue',
    #     ),
    #     dict(
    #         id=177045,
    #         name='Secrets of Draenor Inscription',
    #         icon='inv_misc_book_08',
    #     ),
    # ],
    # # Jewelcrafting
    # 755: [
    #     dict(
    #         id=170700,
    #         name='Taladite Crystal',
    #         icon='inv_jewelcrafting_taladitecrystal',
    #     ),
    #     dict(
    #         id=176087,
    #         name='Secrets of Draenor Jewelcrafting',
    #         icon='inv_misc_book_10',
    #     ),
    # ],
    # # Leatherworking
    # 165: [
    #     dict(
    #         id=171391,
    #         name='Burnished Leather',
    #         icon='inv_misc_startannedleather',
    #     ),
    #     dict(
    #         id=176089,
    #         name='Secrets of Draenor Leatherworking',
    #         icon='inv_misc_book_09',
    #     ),
    # ],
    # # Tailoring
    # 197: [
    #     dict(
    #         id=168835,
    #         name='Hexweave Cloth',
    #         icon='inv_tailoring_hexweavethread',
    #     ),
    #     dict(
    #         id=176058,
    #         name='Secrets of Draenor Tailoring',
    #         icon='inv_misc_book_03',
    #     ),
    # ],
}

all_cooldowns = set()
for spells in profession_cooldowns.values():
    for spell in spells:
        all_cooldowns.add(spell['id'])

# ---------------------------------------------------------------------------
# Bag slots
bag_slots = {}
for line in open(relpath('../raw/bag_slot.data')):
    line = line.strip()
    if line:
        bagid, slots = line.split('=')
        bag_slots[int(bagid)] = int(slots)

bag_data = {
    1: [0, 1, 2, 3, 4],
    2: [-1, 5, 6, 7, 8, 9, 10, 11],
    3: [1, 2],
    5: [-3],
}

# ---------------------------------------------------------------------------
slot_names = {
    1: 'Head',
    2: 'Neck',
    3: 'Shoulders',
    5: 'Chest',
    6: 'Waist',
    7: 'Legs',
    8: 'Feet',
    9: 'Wrists',
    10: 'Hands',
    11: 'Ring 1',
    12: 'Ring 2',
    13: 'Trinket 1',
    14: 'Trinket 2',
    15: 'Cloak',
    16: 'Main-hand',
    17: 'Off-hand',
}

slot_order = [
    16,# main hand
    17,# off hand
    1, # head
    2, # neck
    3, # shoulder
    15,# back
    5, # chest
    #4, # shirt
    #19,# tabard
    9, # wrist
    10,# hands
    6, # waist
    7, # legs
    8, # feet
    11,# finger 1
    12,# finger 2
    13,# trinket 1
    14,# trinket 2
]

enchant_slots = [
    16, # Main-hand
    17, # Off-hand
    11, # Ring 1
    12, # Ring 2
]

socket_bonusids = [
    563,
    1808,
    4802,
]

# ---------------------------------------------------------------------------
# Currency id:object map
currency_map = {}
for currency in Currency.objects.all():
    currency_map[currency.id] = currency

# ---------------------------------------------------------------------------
# Instance name:object map
instance_map = {}
for instance in Instance.objects.all():
    instance_map[instance.name] = instance

# ---------------------------------------------------------------------------
# Realm (region, name):object map
realm_map = {}
for realm in Realm.objects.all():
    realm_map[(realm.region, realm.name)] = realm
    realm_map[(realm.region, realm.name.lower())] = realm
    realm_map[realm.id] = realm

# ---------------------------------------------------------------------------
