from requests_oauthlib import OAuth2Session

from django.conf import settings

REGIONS = [
    'us',
    'eu',
    'kr',
    'tw',
]

BNET_AUTH_URL = 'https://%s.battle.net/oauth/authorize'
BNET_TOKEN_URL = 'https://%s.battle.net/oauth/token'

ACCOUNT_INFO_URL = 'https://%s.battle.net/oauth/userinfo'
CHARACTERS_URL = 'https://%s.api.blizzard.com/wow/user/characters'


class BNetOAuth2(object):
    def __init__(self, region, redirect_uri=None, access_token=None):
        self.region = region

        self.oauth = OAuth2Session(
            settings.BNET_ID,
            redirect_uri=redirect_uri,
            scope=['wow.profile'],
            token=access_token,
        )

    def get_auth_url(self):
        auth_url, state = self.oauth.authorization_url(
            BNET_AUTH_URL % self.region,
        )
        return auth_url, state

    def get_access_token(self, access_code):
        token_data = self.oauth.fetch_token(
            BNET_TOKEN_URL % self.region,
            code=access_code,
            client_secret=settings.BNET_SECRET,
        )
        return token_data

    def get_account_info(self):
        r = self.oauth.get(ACCOUNT_INFO_URL % (self.region))
        return r.status_code, r.json()

    def get_wow_profile(self):
        r = self.oauth.get(CHARACTERS_URL % (self.region))
        return r.status_code, r.json()
