import functools
import operator

from django.contrib.auth.decorators import login_required
from django.db import connection
from django.db.models import Q
from django.http import Http404
from django.shortcuts import *

from auctions.models import Auction
from core.models import BattlePet, Item, Realm
from core.util import format_currency
from intapi.views import _get_bnetaccount

# ---------------------------------------------------------------------------

timeLeft = {
    0: '<30 mins',
    1: '0.5-2 hours',
    2: '2-12 hours',
    3: '12-48 hours',
}

# ---------------------------------------------------------------------------

def _get_good_auctions(auctions):
    if len(auctions) == 0:
        return auctions

    item_ids = set()
    pet_ids = set()
    for auction in auctions:
        if auction.pet_species_id > 0:
            pet_ids.add(auction.pet_species_id)
        else:
            item_ids.add(auction.item_id)

    items = Item.objects.in_bulk(item_ids)
    pets = {}
    for pet in BattlePet.objects.filter(species_id__in=pet_ids):
        pets[pet.species_id] = pet

    good_auctions = []
    for auction in auctions:
        auction.nice_bid = format_currency(auction.bid)
        auction.nice_buyout = format_currency(auction.buyout)
        auction.nice_time_left = timeLeft[auction.time_left]

        if auction.pet_species_id > 0:
            if auction.pet_species_id in pets:
                auction.pet = pets[auction.pet_species_id]
                auction.name = auction.pet.name
                good_auctions.append(auction)
        else:
            auction.item = items.get(auction.item_id)
            if auction.item is not None:
                auction.name = auction.item.name
                good_auctions.append(auction)

    good_auctions.sort(key=lambda a: a.name)

    return good_auctions

# ---------------------------------------------------------------------------

def character(request, region, realm, character):
    realms = list(Realm.objects.filter(Q(region=region, name=realm) | Q(region=region, slug=realm)))
    if len(realms) == 0:
        raise Http404

    auctions = Auction.objects.filter(
        owner_realm=realms[0],
        owner__name__iexact=character,
    ).prefetch_related(
        'owner',
    )

    good_auctions = _get_good_auctions(auctions)

    return render(
        request,
        'auctions/character.html',
        dict(
            realm=realms[0],
            character=character,
            auctions=good_auctions,
        ),
    )

# ---------------------------------------------------------------------------

@login_required
def extra_pets(request):
    return render(
        request,
        'auctions/extra_pets.html',
    )

# ---------------------------------------------------------------------------

@login_required
def mine(request):
    good_auctions = []

    bnetaccount = _get_bnetaccount(request)
    if bnetaccount is not None:
        filters = []
        for character in bnetaccount.characters.all():
            filters.append(Q(owner_realm=character.realm_id, owner__name=character.name))

        if len(filters) > 0:
            auctions = Auction.objects.filter(functools.reduce(operator.or_, filters)).prefetch_related('owner', 'owner_realm')
            good_auctions = _get_good_auctions(auctions)

    return render(
        request,
        'auctions/mine.html',
        dict(
            auctions=good_auctions,
        ),
    )

# ---------------------------------------------------------------------------

@login_required
def missing(request):
    realms = []

    bnetaccount = _get_bnetaccount(request)
    if bnetaccount is not None:
        realms = Realm.objects.filter(character__bnetaccount=bnetaccount).distinct().order_by('name')

    return render(
        request,
        'auctions/missing.html',
        dict(
            realms=realms,
        ),
    )

# ---------------------------------------------------------------------------

def pets(request):
    region = request.GET.get('region')
    pet_name = request.GET.get('pet_name')

    auctions = []
    pet = None
    if region is not None and pet_name is not None:
        try:
            pet = BattlePet.objects.get(name__iexact=pet_name)
        except:
            pass
        else:
            auctions = Auction.objects.filter(
                realm__region=region,
                pet_species_id=pet.species_id,
                buyout__gt=0,
            ).prefetch_related(
                'owner',
                'owner_realm',
            ).order_by(
                'buyout',
            )[:100]

            for auction in auctions:
                auction.nice_buyout = format_currency(auction.buyout)
                auction.nice_time_left = timeLeft[auction.time_left]

    return render(
        request,
        'auctions/pets.html',
        dict(
            auctions=auctions,
            pet=pet,
            region=region,
        ),
    )

# ---------------------------------------------------------------------------
