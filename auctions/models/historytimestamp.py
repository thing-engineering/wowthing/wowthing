from django.db import models

# ---------------------------------------------------------------------------

class HistoryTimestamp(models.Model):
    timestamp = models.DateTimeField()

# ---------------------------------------------------------------------------
