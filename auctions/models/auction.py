from django.contrib.postgres.fields import ArrayField
from django.db import models

from core.fields import BigAutoField

# ---------------------------------------------------------------------------

class Auction(models.Model):
    SHORT_TIME = 0
    MEDIUM_TIME = 1
    LONG_TIME = 2
    VERY_LONG_TIME = 3

    TIME_CHOICES = (
        (SHORT_TIME, 'Short'),
        (MEDIUM_TIME, 'Medium'),
        (LONG_TIME, 'Long'),
        (VERY_LONG_TIME, 'Very Long'),
    )

    id = BigAutoField(primary_key=True)

    realm = models.ForeignKey('core.Realm', db_index=True, related_name='auctions', on_delete=models.CASCADE)
    auction_id = models.IntegerField(db_index=True)

    item_id = models.IntegerField()
    quantity = models.IntegerField()
    bid = models.BigIntegerField()
    buyout = models.BigIntegerField()
    time_left = models.SmallIntegerField(choices=TIME_CHOICES, default=0)

    owner = models.ForeignKey('auctions.CharacterName', db_index=False, on_delete=models.CASCADE)
    owner_realm = models.ForeignKey('core.Realm', db_index=False, related_name='+', on_delete=models.CASCADE)

    context = models.IntegerField()
    rand = models.IntegerField()
    seed = models.BigIntegerField()

    bonus_list = ArrayField(models.IntegerField(), default=list)
    # modifiers = ??? nfi what this is for

    pet_breed_id = models.IntegerField()
    pet_quality_id = models.IntegerField()
    pet_species_id = models.IntegerField()
    pet_level = models.IntegerField()

# ---------------------------------------------------------------------------
