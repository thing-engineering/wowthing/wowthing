from django.db import models

from core.fields import BigAutoField

# ---------------------------------------------------------------------------

class RealmHistory(models.Model):
    id = BigAutoField(primary_key=True)

    timestamp = models.ForeignKey('auctions.HistoryTimestamp', related_name='+', on_delete=models.CASCADE)

    realm = models.ForeignKey('core.Realm', db_index=True, related_name='+', on_delete=models.CASCADE)
    item_id = models.IntegerField(db_index=True)
    pet_species_id = models.IntegerField(db_index=True)

    amount = models.IntegerField()
    min_buyout = models.BigIntegerField()
    max_buyout = models.BigIntegerField()
    mean_buyout = models.BigIntegerField()
    median_buyout = models.BigIntegerField()

# ---------------------------------------------------------------------------
