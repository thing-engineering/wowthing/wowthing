var WoWthing = WoWthing || {};
WoWthing.util = {
    commas: function(num) {
        var parts = num.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    },

    duration: function(t) {
        var secs = t % 60;
        var mins = Math.floor(t / 60) % 60;
        var hours = Math.floor(t / 3600) % 24;
        var days = Math.floor(t / 86400);

        var parts = [];
        if (days) {
            parts.push(days + 'd');
        }
        if (hours) {
            parts.push(hours + 'h');
        }
        if (mins) {
            parts.push(mins + 'm');
        }
        if (secs) {
            parts.push(secs + 's');
        }

        return parts.slice(0, 2).join(' ');
    },

    pad: function(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    },

    keys: function(thing) {
        var keys = [];
        for (var k in thing) {
            keys.push(k);
        }
        keys.sort();
        return keys;
    },

    sane_number: function(n1, n2, unit) {
        var n = Math.max(n1, n2);
        if (n >= 1000000000000) {
            return (n1 / 1000000000000).toFixed(1) + (unit !== false ? 'T' : '');
        }
        else if (n >= 1000000000) {
            return (n1 / 1000000000).toFixed(1) + (unit !== false ? 'B' : '');
        }
        else if (n >= 1000000) {
            return (n1 / 1000000).toFixed(1) + (unit !== false ? 'M' : '');
        }
        else if (n >= 1000) {
            return (n1 / 1000).toFixed(1) + (unit !== false ? 'K' : '');
        }
        else {
            return n1;
        }
    },

    update_hash: function(hash) {
        var base_url = window.location.href.split('#')[0];
        window.location.replace(base_url + '#' + (hash || ''));
    },

    cartesian: function(arg) {
        var r = [],
            max = arg.length - 1;
            // arg = arguments,

        function helper(arr, i) {
            for (var j = 0, l = arg[i].length; j < l; j++) {
                var a = arr.slice(0); // clone arr
                a.push(arg[i][j]);
                if (i === max) {
                    r.push(a);
                } else
                    helper(a, i + 1);
            }
        }
        helper([], 0);
        return r;
    },

    cartesian_object: function(obj) {
        var arrays = [];
        for (var k in obj) {
            arrays.push(obj[k]);
        }
        return WoWthing.util.cartesian(arrays);
    },

    realms: function(region) {
        var realms = [];
        for (var k in WoWthing.data.realms) {
            var realm = WoWthing.data.realms[k];
            if (realm.region === region) {
                realms.push(realm);
            }
        }
        realms.sort(function(a, b) {
            return a.name < b.name ? -1 : 1;
        });
        return realms;
    },

    decode_base64_uint16_array: function(s) {
        var decoded = atob(s),
            ret = [];
        for (var i = 0; i < decoded.length; i += 2) {
            ret.push((decoded.charCodeAt(i)) | (decoded.charCodeAt(i + 1) << 8));
        }
        return ret;
    },

    decode_achievements: function(s) {
        var decoded = atob(s),
            ret = {};
        for (var i = 0; i < decoded.length; i += 6) {
            var key = (decoded.charCodeAt(i) | decoded.charCodeAt(i + 1) << 8),
                value = (decoded.charCodeAt(i + 2) | decoded.charCodeAt(i + 3) << 8 | decoded.charCodeAt(i + 4) << 16 | decoded.charCodeAt(i + 5) << 24);
            ret[key] = value;
        }
        return ret;
    },

    character_race_icon: function(character) {
        var raceIcon = WoWthing.data.races[character.race][character.gender === 1 ? 'female_icon' : 'male_icon'],
            html = '<img src="https://media.openwow.io/icons/16/' + raceIcon + '.png"';
        html += ' data-tippy-content="' + (character.gender === 1 ? 'Female ' : 'Male ') + WoWthing.data.races[character.race].name + '">';//</div>';
        return html;
    },

    character_class_icon: function(character) {
        var html = '<img src="https://media.openwow.io/icons/16/' + WoWthing.data.classes[character.cls].icon + '.png"';
        html += ' data-tippy-content="' + WoWthing.data.classes[character.cls].name + '">';
        return html;
    },

    character_armory_link: function(character, region, includeRealm) {
        var realm = WoWthing.data.realms[character.realm_id],
            name = (includeRealm ? character.name + '-' + realm.name : character.name);
        
        return '<a href="https://worldofwarcraft.com/' + WoWthing.data.region_armory[region] + '/character/' + realm.slug + '/' + character.name.toLowerCase() + '">' + name + '</a>';
    },

    get_id_img: function(type, id, px, cls) {
        return '<img src="https://media.wowdb.com/icons/' + type + '/' + px + '/' + id + '.jpg" width="' + px + '" height="' + px + '"' + (cls !== undefined ? ' class="' + cls + '"' : '') + '>';
    },

    get_icon_img: function(icon, px, cls) {
        var size = (px === 16 ? 'small' : (px === 36 ? 'medium' : 'large')),
            icon = (icon || 'inv_misc_questionmark').split('.')[0];
        return '<img src="https://media.openwow.io/icons/' + size + '/' + icon + '.png" width="' + px + '" height="' + px + '"' + (cls !== undefined ? ' class="' + cls + '"' : '') + '>';
    },

    get_openwow_icon: function(icon, px, cls) {
        var icon = (icon || 'inv_misc_questionmark').split('.')[0];
        return '<img src="https://media.openwow.io/icons/' + px + '/' + icon + '.png" width="' + px + '" height="' + px + '"' + (cls !== undefined ? ' class="' + cls + '"' : '') + '>';
    },

    get_garrison_img: function(icon, cls) {
        return '<img src="https://media-azeroth.cursecdn.com/wow/garrisons/26032/' + icon + '.jpg" width="36" height="36"' + (cls !== undefined ? ' class="' + cls + '"' : '') + '>';
    },

    /* item is a weird object:
       - i: item ID
       - q: quality
       - e: extra JSON - {b: bonus IDs, e: enchant ID, g: gem string}
    */
    get_item_link: function(item) {
        var extra = JSON.parse(item.e || '{}'),
            params = [];
        if (extra.b) {
            params.push('bonusIDs=' + extra.b.join(','));
        }
        if (extra.e) {
            params.push('enchantment=' + extra.e);
        }
        if (extra.g) {
            params.push('gems=' + extra.g.replace(/:/g, ','));
        }

        var html = '<a class="quality' + item.q +'" href="https://' + WoWthing.data.wowdb_base + '/items/' + item.i;
        if (params.length > 0) {
            html += '?' + params.join('&');
        }
        html += '">';
        return html;
    },

    /*

    */
    get_equipped_item: function(item, slot, quality, itemLevel, playerClass, extraClass, iconOverride, itemClass) {
        var html = '',
            parts = [];

        if (item.azerite && item.azerite.length > 0) {
            parts.push('azerite=' + playerClass + ':' + item.context + ':' + item.azerite.join(':'));
        }
        if (item.bonuses.length > 0) {
            parts.push('bonusIDs=' + item.bonuses.join(','));
        }
        if (item.enchant) {
            parts.push('enchantment=' + item.enchant);
        }
        if (item.upgrade && item.upgrade[0] > 0) {
            parts.push('upgradeNum=' + item.upgrade[0]);
        }
        
        if (item.relics) {
            parts.push('gems=' + item.relics.join(','));
        }
        else if (item.gems) {
            parts.push('gems=' + item.gems);
        }

        html += '<div class="icon-overlay' + (extraClass ? ' ' + extraClass : '') + '">';

        html += '<a href="https://' + WoWthing.data.wowdb_base + '/items/' + item.item_id;
        if (parts.length > 0) {
            html += '?' + parts.join("&");
        }
        html += '" target="_blank" rel="nofollow noopener noreferrer">';

        if (iconOverride) {
            html += WoWthing.util.get_icon_img(iconOverride, 36);
        }
        else {
            html += WoWthing.util.get_id_img('items', item.item_id, 36, 'border-quality' + quality);
        }

        if (itemLevel !== undefined) {
            html += '<span class="br">' + itemLevel + '</span>';
        }

        if (WoWthing.util.is_missing_enchant(item, slot, itemClass)) {
            html += '<div class="missing-enchant"></div>';
        }

        if (WoWthing.util.is_missing_gem(item)) {
            html += '<div class="missing-gem"></div>';
        }

        html += '</a>';

        html += '</div>';
        return html;
    },

    is_missing_enchant: function(item, slot, itemClass) {
        if (WoWthing.data.enchant_slots.indexOf(slot) >= 0 && item.enchant === 0) {
            // Off-hand (slot 17) armor (class 4) doesn't need an enchant
            if (slot === 17 && itemClass === 4) {
                return false;
            }
            return true;
        }
        return false;
    },

    is_missing_gem: function(item) {
        if (item.gems === '') {
            for (var i = 0; i < item.bonuses.length; i++) {
                if (WoWthing.data.socket_bonusids.indexOf(item.bonuses[i]) >= 0) {
                    return true;
                }
            }
        }
        return false;
    },

    fancy_tooltip: function(content, title) {
        if (!content) {
            return '';
        }

        var html = ' data-tippy-content="';
        if (title !== undefined) {
            html += "<div class='fancy-title'>" + title + '</div>';
        }
        html += "<div class='fancy-content'>" + content + '</div>';
        html += '" data-tippy-placement="right"';
        return html;
    },
};
