var WoWthing = WoWthing || {};
WoWthing.Achievement = function(data) {
    for (var i = 0; i < arguments.length; i++) {
        this[achievement_keys[i]] = arguments[i];
    }
};

WoWthing.Achievement.prototype.check_faction = function(character) {
    if (this.faction !== undefined &&
        (
            (this.faction === 0 && character.side === 'alliance') ||
            (this.faction === 1 && character.side === 'horde') ||
            this.faction === 2
        )
    ) {
        return true;
    }
    else {
        return false;
    }
};

var achievement_keys = [
    'id',
    'title',
    'description',
    'icon',
    'points',
    'faction',
    'accountWide',
    'requiredCriteria',
    'criteria',
];
