var WoWthing = WoWthing || {};
WoWthing.teams = (function() {
    var roles = {
            0: {id: 0, name: 'Special snowflake', icon: 'inv_pet_snowman'},
            1: {id: 1, name: 'Tank', icon: 'ability_warrior_defensivestance'},
            2: {id: 2, name: 'Healer', icon: 'spell_nature_resistnature'},
            3: {id: 3, name: 'Melee DPS', icon: 'ability_dualwield'},
            4: {id: 4, name: 'Ranged DPS', icon: 'spell_fire_flamebolt'},
        },
        class_data = {
             1: {armor: 'Plate', roles: [0, 1, 3]}, // Warrior
             2: {armor: 'Plate', roles: [0, 1, 2, 3]}, // Paladin
             3: {armor: 'Mail',  roles: [0, 3, 4]}, // Hunter
             4: {armor: 'Leather', roles: [0, 3]}, // Rogue
             5: {armor: 'Cloth', roles: [0, 2, 4]}, // Priest
             6: {armor: 'Plate', roles: [0, 1, 3]}, // Death Knight
             7: {armor: 'Mail', roles: [0, 2, 3, 4]}, // Shaman
             8: {armor: 'Cloth', roles: [0, 4]}, // Mage
             9: {armor: 'Cloth', roles: [0, 4]}, // Warlock
            10: {armor: 'Leather', roles: [0, 1, 2, 3]}, // Monk
            11: {armor: 'Leather', roles: [0, 1, 2, 3, 4]}, // Druid
            12: {armor: 'Leather', roles: [0, 1, 3]}, // Demon Hunter
        },
        groupby = 'none',
        highlight = 'none',
        show = 'gear',
        roleChanges = {},
        gear_template,
        mythicplus_template;

    var view_onload = function() {
        gear_template = Handlebars.getTemplate('team_gear');
        mythicplus_template = Handlebars.getTemplate('team_mythicplus');

        $('.js-radio-show').change(show_change);
        $('.js-radio-groupby').change(groupby_change);
        $('.js-radio-highlight').change(highlight_change);

        var $ir = $('#input-realm');
        if ($ir.length > 0) {
            var realms = WoWthing.util.realms(WoWthing.teams.data.region),
                html = '';
            for (var i = 0; i < realms.length; i++) {
                var realm = realms[i];
                html += '<option value="' + realm.id + '">' + realm.name + '</option>';
            }
            $ir.html(html);
            if (WoWthing.teams.data.default_realm !== null) {
                $ir.val(WoWthing.teams.data.default_realm);
            }
        }

        if (WoWthing.teams.data.owner) {
            $('body').on('click', '.js-edit-role', function(e) {
                if (e.preventDefault) {
                    e.preventDefault();
                }

                var $this = $(this),
                    memberId = $this.closest('tr').data('member-id');

                $this.closest('.dropright').removeClass('show');
                $this.closest('.dropdown-menu').removeClass('show');

                if (roleChanges[memberId] === undefined) {
                    roleChanges[memberId] = [100, 100];
                }

                roleChanges[memberId][parseInt($this.data('position'))] = $this.data('role');
                update_role_changes();

                return false;
            });
        }

        var hash = document.location.hash.replace('#', ''),
            hashParts = hash.split(';');
        if (hashParts.length === 3) {
            if (hashParts[0].match(/^(?:gear|mythicplus)$/)) {
                show = hashParts[0];
            }
            if (hashParts[1].match(/^(?:none|class|primaryrole|armortype|note)$/)) {
                groupby = hashParts[1];
            }
            if (hashParts[2].match(/^(?:none|problems)$/)) {
                highlight = hashParts[2];
            }
        }

        $('.js-radio-show[value=' + show + ']').prop('checked', true);
        $('.js-radio-groupby[value=' + groupby + ']').prop('checked', true);
        $('.js-radio-highlight[value=' + highlight + ']').prop('checked', true);

        register_helpers();

        fix_equipped();
        build_table();
    };

    var update_role_changes = function() {
        var parts = [];
        for (var key in roleChanges) {
            parts.push(key + ',' + roleChanges[key][0] + ',' + roleChanges[key][1]);
        }
        $('#input-edit-roles').val(parts.join(';'));
    };

    var create_onload = function() {
        $('#region').on('change', function() {
            var region = $(this).val(),
                realms = WoWthing.util.realms(region),
                html = '';
            for (var i = 0; i < realms.length; i++) {
                var realm = realms[i];
                html += '<option value="' + realm.id + '">' + realm.name + '</option>';
            }
            $('#realm').html(html);
        }).change();
    };

    var groupby_change = function() {
        groupby = $('.js-radio-groupby:checked').val();
        build_table();
        save_hash();
    };

    var highlight_change = function() {
        $('.gear-image.faded').removeClass('faded');
        
        highlight = $('.js-radio-highlight:checked').val();
        switch (highlight) {
            case 'problems':
                $('.gear-image:not(.js-problem)').addClass('faded');
                break;
        }

        save_hash();
    };

    var show_change = function() {
        show = $('.js-radio-show:checked').val();
        build_table();
        save_hash();
    }

    var save_hash = function() {
        WoWthing.util.update_hash(show + ';' + groupby + ';' + highlight);
    };

    var owner_role = function(member, role, text) {
        var classRoles = class_data[member.character.cls].roles,
            tooltip = text + ' role<br>' + role.name + '<br><br>Click to edit',
            html = '';

        html += '<div class="dropright">';
        html += '<img src="https://media.openwow.io/icons/16/' + role.icon + '.png" data-tippy-content="' + tooltip + '" class="dropdown-toggle" data-toggle="dropdown" data-offset="0,5px">';
        html += '<div class="dropdown-menu">';
        for (var i = 0; i < classRoles.length; i++) {
            html += '<a href="#" class="dropdown-item js-edit-role" data-position="' + (text === 'Primary' ? 0 : 1) + '" data-role="' + classRoles[i] + '">';
            html += roles[classRoles[i]].name + '</a>';
        }
        html += '</div></div>';
        return html;
    };

    var show_secondary_role = function(member) {
        var roles = class_data[member.character.cls].roles;
        // If they can't be a tank or healer they can't really be a snowflake
        if (roles.indexOf(1) === -1 && roles.indexOf(2) === -1) {
            return false;
        }
        return true;
    }

    var register_helpers = function() {
        Handlebars.registerHelper('characterName', function() {
            var html = '<td class="team-name"><div class="flex-wrap">';

            // Icons
            html += '<div class="team-icons">';
            html += WoWthing.util.character_race_icon(this.character);
            html += WoWthing.util.character_class_icon(this.character);
            html += '</div>';

            // Roles
            var primary = roles[this.primary_role],
                secondary = roles[this.secondary_role];
            html += '<div class="team-icons">';
            if (WoWthing.teams.data.owner) {
                html += owner_role(this, primary, 'Primary');
                if (show_secondary_role(this)) {
                    html += owner_role(this, secondary, 'Secondary');
                }
            }
            else {
                html += '<img src="https://media.openwow.io/icons/16/' + primary.icon + '.png" data-tippy-content="Primary role<br>' + primary.name + '">';
                if (show_secondary_role(this)) {
                    html += '<img src="https://media.openwow.io/icons/16/' + secondary.icon + '.png" data-tippy-content="Secondary role<br>' + secondary.name + '">';
                }
            }
            html += '</div>';

            // Name/Note
            html += '<div class="team-character">';
            html += WoWthing.util.character_armory_link(this.character, WoWthing.teams.data.region, true);
            if (this.note) {
                html += '<br><span>' + this.note + '</span>';
            }
            html += '</div>';

            html += '</div></td>';
            return new Handlebars.SafeString(html);
        });
        // Azerite level
        Handlebars.registerHelper('azeriteLevel', function() {
            var html = '<div data-tippy-content="Azerite level">';
            html += WoWthing.util.get_openwow_icon('inv_heartofazeroth', 16) + this.character.azerite_level;
            html += '</div>';
            return new Handlebars.SafeString(html);
        });
        // Status messages
        Handlebars.registerHelper('statusMessages', function() {
            var html = '<td class="team-status">',
                parts = [];

            // Items!
            var enchants = 0,
                enchantList = [],
                gems = 0,
                gemList = [],
                setItems = {};
            for (var i = 0; i < WoWthing.data.slot_order.length; i++) {
                var slot = WoWthing.data.slot_order[i],
                    item = this.character.equipped[slot];

                if (item === undefined) {
                    continue;
                }

                var itemData = WoWthing.teams.data.items[item.item_id];
                if (itemData === undefined) {
                    continue;
                }
                
                // Enchants
                if (WoWthing.util.is_missing_enchant(item, slot, WoWthing.teams.data.items[item.item_id].c))
                {
                    enchants++;
                    enchantList.push(WoWthing.data.slot_names[slot]);
                }

                // Gems
                if (WoWthing.util.is_missing_gem(item)) {
                    gems++;
                    gemList.push(WoWthing.data.slot_names[slot]);
                }
            }

            if (enchants > 0)
            {
                var temp = '<span class="text-danger" rel="popover" data-content="' + enchantList.join('<br>') + '">';
                temp += enchants + ' enchant' + (enchants > 1 ? 's' : '') + '!</span>';
                parts.push(temp);
            }
            if (gems > 0) {
                var temp = '<span class="text-danger" rel="popover" data-content="' + gemList.join('<br>') + '">';
                temp += gems + ' gem' + (gems > 1 ? 's' : '') + '!</span>';
                parts.push(temp);
            }

            html += parts.join('<br>') + '</td>';
            return new Handlebars.SafeString(html);
        });
        // Gear
        Handlebars.registerHelper('equippedItems', function() {
            var html = '';

            for (var i = 0; i < WoWthing.data.slot_order.length; i++) {
                var slot = WoWthing.data.slot_order[i],
                    item = this.character.equipped[slot];

                html += '<td class="team-item">';
                if (item !== undefined) {
                    var item_data = WoWthing.teams.data.items[item.item_id],
                        extraClass = '';
                    
                    if (WoWthing.util.is_missing_enchant(item, slot, item_data.c) || WoWthing.util.is_missing_gem(item)) {
                        extraClass = 'js-problem';
                    }

                    html += WoWthing.util.get_equipped_item(item, slot, item.quality, item.level, this.character.cls, extraClass, null, item_data.c);
                }
                html += '</td>';
            }

            return new Handlebars.SafeString(html);
        });
        // Mythic+
        Handlebars.registerHelper('characterKeystoneMax', function() {
            var shiny = 1;
            if (this.character.keystone_max >= 15) {
                shiny = 5;
            }
            if (this.character.keystone_max >= 10) {
                shiny = 4;
            }
            else if (this.character.keystone_max >= 7) {
                shiny = 3;
            }
            else if (this.character.keystone_max >= 4) {
                shiny = 2;
            }
            var html = '<td class="team-keystone quality' + shiny + '" data-tippy-content="Max keystone completed this week">' + this.character.keystone_max + '</td>';
            return new Handlebars.SafeString(html);
        });
        Handlebars.registerHelper('mythicPlus', function() {
            var html = '';
            for (var i = 0; i < WoWthing.data.keystone_order.length; i++) {
                var dungeon_id = WoWthing.data.keystone_order[i],
                    runs = this.character.mythicplus[dungeon_id] || [],
                    title = WoWthing.data.keystone_maps[dungeon_id][1];

                html += '<td class="team-mythic' + (runs.length > 0 ? '' : ' faded-missing') + '" data-tippy-content="' + title +'">';
                html += '<div class="icon-overlay">';
                html += '<img src="/static/img/dungeons/' + dungeon_id + '.jpg" width="40" height="40">';

                var max_level = 0;
                for (var j = 0; j < runs.length; j++) {
                    var run = runs[j];
                    if (run.level > max_level) {
                        max_level = run.level;
                        if (run.in_time) {
                            html += '<span class="highlight br">' + run.level + '</span>';
                        }
                        else {
                            html += '<span class="warning bl">' + run.level + '</span>';
                        }
                    }
                }

                html += '</div>';
                html += '</td>';
            }
            html += '<td class="team-spacer"></td>';

            return new Handlebars.SafeString(html);
        });
    };

    var group_by = {
        'armortype': function(member) { return class_data[member.character.cls].armor; },
        'class': function(member) { return WoWthing.data.classes[member.character.cls].name; },
        'note': function(member) { return member.note; },
        'primaryrole': function(member) { return roles[member.primary_role].name; },
    };

    var group_members = function() {
        var ret = {};
        for (var i = 0; i < WoWthing.teams.data.members.length; i++) {
            var member = WoWthing.teams.data.members[i],
                key = group_by[groupby](member);
            if (ret[key] === undefined) {
                ret[key] = [];
            }
            ret[key].push(member);
        }
        return ret;
    };

    var build_table = function() {
        var data = WoWthing.teams.data,
            html = '';

        if (groupby === 'none') {
            html += render_table(WoWthing.teams.data.members);
        }
        else {
            var grouped = group_members(WoWthing.teams.data.members),
                keys = WoWthing.util.keys(grouped);
            for (var i = 0; i < keys.length; i++) {
                var thing = keys[i];
                html += render_table(grouped[thing], thing);
            }
        }

        $('#team-box').html(html);
        highlight_change();

        WoWthing.home.activate_content_stuff();
    };

    var render_table = function(members, text) {
        var data = WoWthing.teams.data,
            html = '';

        if (text !== undefined) {
            html += '<h3 style="margin-top: 0">' + text + ' (' + members.length + ')</h3>';
        }

        var view_template = (show === 'mythicplus' ? mythicplus_template : gear_template);
        html += view_template({
            members: members,
            items: data.items,
            owner: data.owner,
            team_id: data.team_id,
        });
        return html;
    };

    var fix_equipped = function() {
        for (var i = 0; i < WoWthing.teams.data.members.length; i++) {
            var character = WoWthing.teams.data.members[i].character,
                equipped = {},
                total_ilvl = 0;

            for (var j = 0; j < character.items.length; j++) {
                var eq = character.items[j];
                // Skip shirts and tabards
                if (eq.s === 4 || eq.s === 19) {
                    continue;
                }

                var extra = JSON.parse(eq.e);

                equipped[eq.s] = {
                    item_id: eq.i,
                    context: eq.c,
                    level: eq.l,
                    quality: eq.q,
                    azerite: extra.a || [],
                    bonuses: extra.b,
                    enchant: extra.e,
                    gems: extra.g.replace(/:/g, ','),
                    relics: extra.r,
                    upgrade: extra.u,
                };
                
                total_ilvl += equipped[eq.s].level;
            }
            // Two-handed needs to be counted twice
            if (equipped[16] && !equipped[17]) {
                total_ilvl += equipped[16].level;
            }

            character.equipped = equipped;
            character.average_ilvl = (total_ilvl / 16).toFixed(1);
        }
    };

    // Public stuff
    return {
        create_onload: create_onload,
        view_onload: view_onload,
    };
})();
