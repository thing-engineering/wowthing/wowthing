# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-04-25 06:47
from __future__ import unicode_literals

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('thing', '0012_auto_20180424_2235'),
    ]

    operations = [
        migrations.AddField(
            model_name='character',
            name='balance_mythic15',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='character',
            name='balance_unleashed_monstrosities',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.SmallIntegerField(), default=list, size=None),
        ),
    ]
