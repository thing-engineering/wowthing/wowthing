# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-15 22:44
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('thing', '0005_characterfollower_is_active'),
    ]

    operations = [
        migrations.AddField(
            model_name='characterfollower',
            name='max_vitality',
            field=models.SmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='characterfollower',
            name='vitality',
            field=models.SmallIntegerField(default=0),
        ),
    ]
