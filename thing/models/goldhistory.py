from django.db import models

# ---------------------------------------------------------------------------

class GoldHistory(models.Model):
    bnetaccount = models.ForeignKey('core.BNetAccount', blank=True, null=True, related_name='+', on_delete=models.CASCADE)
    realm = models.ForeignKey('core.Realm', on_delete=models.CASCADE)

    time = models.DateTimeField()
    gold = models.BigIntegerField()

# ---------------------------------------------------------------------------
