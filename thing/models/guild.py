from django.db import models

# ---------------------------------------------------------------------------

class Guild(models.Model):
    name = models.CharField(max_length=30)
    realm = models.ForeignKey('core.Realm', on_delete=models.CASCADE)

# ---------------------------------------------------------------------------
