from django.db import models

# ---------------------------------------------------------------------------

class CharacterCooldown(models.Model):
    character = models.ForeignKey('Character', on_delete=models.CASCADE)

    spell_id = models.IntegerField()
    expires = models.DateTimeField(blank=True, null=True)

# ---------------------------------------------------------------------------
