from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField
from django.db import models

# ---------------------------------------------------------------------------

class Character(models.Model):
    bnetaccount = models.ForeignKey('core.BNetAccount', blank=True, null=True, related_name='characters', on_delete=models.SET_NULL)
    #bnetaccount = models.IntegerField(blank=True, null=True)
    tag = models.ForeignKey('CharacterTag', blank=True, null=True, on_delete=models.SET_NULL)
    guild = models.ForeignKey('Guild', blank=True, null=True, on_delete=models.SET_NULL)

    name = models.CharField(max_length=16)
    realm = models.ForeignKey('core.Realm', on_delete=models.CASCADE)
    cls = models.ForeignKey('core.CharacterClass', on_delete=models.CASCADE)
    race = models.ForeignKey('core.CharacterRace', on_delete=models.CASCADE)
    gender = models.SmallIntegerField(default=0)
    level = models.SmallIntegerField(default=1)
    thumbnail = models.CharField(max_length=128)

    honorable_kills = models.IntegerField(default=0)
    pvp_prestige = models.SmallIntegerField(default=0)
    pvp_level = models.SmallIntegerField(default=0)
    pvp_honor_current = models.SmallIntegerField(default=0)
    pvp_honor_max = models.SmallIntegerField(default=0)

    copper = models.BigIntegerField(default=0)
    flight_speed = models.SmallIntegerField(default=0)
    ground_speed = models.SmallIntegerField(default=0)
    played_total = models.IntegerField(default=0)
    played_level = models.IntegerField(default=0)
    garrison_level = models.SmallIntegerField(default=0)

    current_xp = models.IntegerField(default=0)
    level_xp = models.IntegerField(default=0)
    rested_xp = models.IntegerField(default=0)
    resting = models.BooleanField(default=False)

    keystone_id = models.SmallIntegerField(default=0)
    keystone_level = models.SmallIntegerField(default=0)
    keystone_max = models.SmallIntegerField(default=0)
    keystone_period = models.SmallIntegerField(default=0)

    hidden_dungeons = models.SmallIntegerField(default=0)
    hidden_kills = models.SmallIntegerField(default=0)
    hidden_world_quests = models.SmallIntegerField(default=0)
    
    balance_mythic15 = models.BooleanField(default=False)
    balance_unleashed_monstrosities = ArrayField(models.SmallIntegerField(), default=list)

    bigger_fish_to_fry = ArrayField(models.SmallIntegerField(), default=list)

    azerite_level = models.SmallIntegerField(default=0)
    azerite_current_xp = models.BigIntegerField(default=0)
    azerite_level_xp = models.BigIntegerField(default=0)
    azerite_essences = ArrayField(models.IntegerField(), default=list)

    # API
    count_404 = models.IntegerField(default=0)
    last_modified = models.IntegerField(default=0)
    # Worker backend
    last_api_update = models.DateTimeField(blank=True, null=True, db_index=True)
    # Character data
    last_char_update = models.DateTimeField(blank=True, null=True)
    # Item data
    last_bag_update = models.DateTimeField(blank=True, null=True)
    last_bank_update = models.DateTimeField(blank=True, null=True)
    last_void_update = models.DateTimeField(blank=True, null=True)
    # Misc data
    last_mount_update = models.DateTimeField(blank=True, null=True)
    last_pet_update = models.DateTimeField(blank=True, null=True)
    last_reputation_update = models.DateTimeField(blank=True, null=True)
    last_world_quest_update = models.DateTimeField(blank=True, null=True)
    # Garrison data
    last_building_update = models.DateTimeField(blank=True, null=True)
    last_cache_check = models.DateTimeField(blank=True, null=True)
    last_follower_update = models.DateTimeField(blank=True, null=True)
    last_mission_update = models.DateTimeField(blank=True, null=True)

    # -------------------------------------------------------------------------

    def get_thumbnail_url(self):
        region = self.realm.region
        return '//%s.battle.net/static-render/%s/%s' % (region, region, self.thumbnail)

    def get_copper(self):
        return self.copper % 100

    def get_silver(self):
        return (self.copper / 100) % 100

    def get_gold(self):
        return self.copper / 10000

    def get_money(self):
        rest, copper = divmod(self.copper, 100)
        gold, silver = divmod(rest, 100)

        parts = []
        if gold:
            parts.append('%dg' % (gold))
        if silver:
            parts.append('%ds' % (silver))
        if copper or not (gold or silver):
            parts.append('%dc' % (copper))

        return ' '.join(parts)

# ---------------------------------------------------------------------------
