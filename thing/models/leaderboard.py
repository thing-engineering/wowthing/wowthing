from django.db import models

from core.models import BNetAccount

# ---------------------------------------------------------------------------

class Leaderboard(models.Model):
    bnetaccount = models.ForeignKey('core.BNetAccount', related_name='leaderboard', on_delete=models.CASCADE)

    achievement_total = models.IntegerField(default=0)
    achievement_alliance = models.IntegerField(default=0)
    achievement_horde = models.IntegerField(default=0)
    achievement_feat = models.IntegerField(default=0)

    reputation_exalted = models.IntegerField(default=0)

    honorable_kill_total = models.IntegerField(default=0)

    mount_unique = models.IntegerField(default=0)

    pet_unique = models.IntegerField(default=0)
    pet_score = models.IntegerField(default=0)

    toy_unique = models.IntegerField(default=0)

    xp_total = models.BigIntegerField(default=0)

# ---------------------------------------------------------------------------
# Magical hook so this gets called when a new bnetaccount is created
def create_leaderboard(sender, instance, created, **kwargs):
    if created:
        Leaderboard.objects.create(bnetaccount=instance)

models.signals.post_save.connect(create_leaderboard, sender=BNetAccount)

# ---------------------------------------------------------------------------
