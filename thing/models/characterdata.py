from django.contrib.postgres.fields import ArrayField
from django.db import models

from thing.models import Character

# ---------------------------------------------------------------------------

class CharacterData(models.Model):
    character = models.ForeignKey('thing.Character', related_name='data', on_delete=models.CASCADE)

    achievements = ArrayField(models.IntegerField(), default=list)
    achievements_timestamp = ArrayField(models.BigIntegerField(), default=list)
    criteria = ArrayField(models.IntegerField(), default=list)
    criteria_quantity = ArrayField(models.BigIntegerField(), default=list)

    mounts = ArrayField(models.IntegerField(), default=list)
    mounts_extra = ArrayField(models.IntegerField(), default=list)

    quests = ArrayField(models.IntegerField(), default=list)
    quests_extra = ArrayField(models.IntegerField(), default=list)
    quests_md5 = models.CharField(max_length=32, default='')

# ---------------------------------------------------------------------------
# Magical hook so this gets called when a new character is created
def create_characterdata(sender, instance, created, **kwargs):
    if created:
        CharacterData.objects.create(character=instance)

models.signals.post_save.connect(create_characterdata, sender=Character)

# ---------------------------------------------------------------------------
