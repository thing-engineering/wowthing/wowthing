from django.db import models

# ---------------------------------------------------------------------------

class CharacterItem(models.Model):
    BAGS_LOCATION = 1
    BANK_LOCATION = 2
    VOID_STORAGE_LOCATION = 3
    EQUIPPED_LOCATION = 4
    REAGENT_BANK_LOCATION = 5

    LOCATION_CHOICES = (
        (BAGS_LOCATION, 'Bags'),
        (BANK_LOCATION, 'Bank'),
        (VOID_STORAGE_LOCATION, 'Void Storage'),
        (EQUIPPED_LOCATION, 'Equipped'),
        (REAGENT_BANK_LOCATION, 'Reagent Bank'),
    )

    character = models.ForeignKey('Character', related_name='items', on_delete=models.CASCADE)
    item = models.ForeignKey('core.Item', related_name='+', on_delete=models.CASCADE)
    item_level = models.SmallIntegerField(default=0)
    quality = models.SmallIntegerField(default=0)

    count = models.IntegerField()
    location = models.SmallIntegerField(choices=LOCATION_CHOICES)
    container = models.IntegerField(default=0)
    slot = models.IntegerField(default=0)
    context = models.SmallIntegerField(default=0)

    extra = models.CharField(max_length=255, default='')

# ---------------------------------------------------------------------------
