from django.contrib.postgres.fields import ArrayField
from django.db import models

# ---------------------------------------------------------------------------

class CharacterFollower(models.Model):
    character = models.ForeignKey('Character', on_delete=models.CASCADE)
    follower = models.ForeignKey('core.GarrisonFollower', on_delete=models.CASCADE)

    level = models.SmallIntegerField(default=0)
    quality = models.SmallIntegerField(default=0)
    current_xp = models.IntegerField(default=0)
    level_xp = models.IntegerField(default=0)
    item_level = models.SmallIntegerField(default=0)
    vitality = models.SmallIntegerField(default=0)
    max_vitality = models.SmallIntegerField(default=0)
    is_troop = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)

    abilities = ArrayField(models.SmallIntegerField(), default=list)
    equipment = ArrayField(models.SmallIntegerField(), default=list)

# ---------------------------------------------------------------------------
