from django.db import models

# ---------------------------------------------------------------------------

_level_modifiers = {
    7: 42000,
    6: 21000,
    5: 9000,
    4: 3000,
    3: 0,
    2: -3000,
    1: -6000,
    0: -42000,
}

class CharacterReputation(models.Model):
    character = models.ForeignKey('Character', related_name='reputations', on_delete=models.CASCADE)
    faction = models.ForeignKey('core.Faction', on_delete=models.CASCADE)

    level = models.SmallIntegerField()
    current = models.IntegerField()
    max_value = models.IntegerField()
    paragon_value = models.SmallIntegerField(default=0)
    paragon_max = models.SmallIntegerField(default=0)
    paragon_ready = models.BooleanField(default=False)

    def get_absolute(self):
        return self.current + _level_modifiers[self.level]

# ---------------------------------------------------------------------------
