import datetime
import functools
import json
import operator
import random
import re

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.shortcuts import *
from django.urls import reverse
from django.utils import timezone

import data.manual as manual_data
from bnet_oauth import BNetOAuth2
from core.models import BNetAccount, Realm
from core.util import render_error
from thing.models import Character, Guild

# ---------------------------------------------------------------------------

def get_redirect_uri():
    return settings.BNET_REDIRECT_BASE % (reverse(oauth_redirect))

# ---------------------------------------------------------------------------

ALPHANUMERIC = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890'
BAD_NAME_RE = re.compile(r'^.+[A-Z0-9].*?$')

REGIONS = (
    ('us', 'North America'),
    ('eu', 'Europe'),
    ('kr', 'South Korea'),
    ('tw', 'Taiwain'),
)

# ---------------------------------------------------------------------------

@login_required
def oauth(request):
    account_qs = BNetAccount.objects.filter(
        user=request.user,
    )

    return render(
        request,
        'thing/oauth.html',
        dict(
            accounts=account_qs,
            regions=REGIONS,
        ),
    )

# ---------------------------------------------------------------------------

@login_required
def oauth_delete(request, account_id):
    try:
        bna = BNetAccount.objects.get(pk=account_id, user=request.user)
    except BNetAccount.DoesNotExist:
        raise PermissionDenied

    if request.GET.get('confirm', '') == request.session.get('oauth_delete_confirm', None):
        if request.GET.get('delete_characters', '') != 'on':
            bna.characters.update(bnetaccount=None)

        bna.delete()

        del request.session['oauth_delete_confirm']

        return redirect(reverse(oauth))

    else:
        request.session['oauth_delete_confirm'] = ''.join(random.sample(ALPHANUMERIC, 32))

        return render(
            request,
            'thing/oauth_delete.html',
            dict(
                account=bna,
                confirm=request.session['oauth_delete_confirm'],
            )
        )

# ---------------------------------------------------------------------------

@login_required
def oauth_begin(request):
    region = request.GET.get('region', 'us')
    if not [r for r in REGIONS if r[0] == region]:
        region = 'us'

    request.session['region'] = region

    b = BNetOAuth2(
        region,
        redirect_uri=get_redirect_uri(),
    )

    url, state = b.get_auth_url()
    request.session['bnet_state'] = state

    return redirect(url)

# ---------------------------------------------------------------------------

@login_required
def oauth_redirect(request):
    original_state = request.session.get('bnet_state', None)
    provided_state = request.GET.get('state', None)
    region = request.session.get('region', None)
    if None in (original_state, provided_state, region) or original_state != provided_state:
        return render_error(request, 'OAuth error', 'Invalid state parameter')

    # Fetch the access token
    b = BNetOAuth2(
        region,
        redirect_uri=get_redirect_uri(),
    )

    access_token = b.get_access_token(request.GET.get('code'))

    # Go get user information
    b = BNetOAuth2(
        region,
        redirect_uri=get_redirect_uri(),
        access_token=access_token,
    )

    try:
        status, account_info = b.get_account_info()
        if status != 200:
            return render_error(request, 'OAuth error', 'Unable to retrieve account information')
    except:
        return render_error(request, 'OAuth error', 'Unable to retrieve account information')

    token_json = json.dumps(access_token)
    token_expires = timezone.now() + datetime.timedelta(seconds=access_token['expires_in'])

    # See if this account exists already
    try:
        account = BNetAccount.objects.get(
            account_id=account_info['id'],
            region=region,
        )
    # Doesn't exist, create
    except BNetAccount.DoesNotExist:
        account = BNetAccount(
            user=request.user,
            region=region,
            account_id=account_info['id'],
            battletag=account_info['battletag'] or '??',
            token_json=json.dumps(access_token),
            token_expires=token_expires,
            last_update=timezone.now(),
        )
        account.save()

        messages.add_message(request, messages.SUCCESS, 'Account %s added successfully!' % account.battletag)
    # wtf?
    except KeyError:
        raise ValueError(repr(access_token))
    # Does exist, check ownership then update
    else:
        if account.user != request.user:
            return render_error(request, "OAuth error", "That account already exists for another user!")

        account.region = region
        account.battletag = account_info['battletag']
        account.token_json = token_json
        account.token_expires = token_expires
        account.last_update = timezone.now()
        account.save(update_fields=['region', 'battletag', 'token_json', 'token_expires', 'last_update'])

        messages.add_message(request, messages.SUCCESS, 'Account %s updated successfully!' % account.battletag)

    _update_oauth(account)

    return redirect(reverse(oauth))

# ---------------------------------------------------------------------------

def _update_oauth(account):
    access_token = json.loads(account.token_json)

    b = BNetOAuth2(
        account.region,
        redirect_uri=get_redirect_uri(),
        access_token=access_token,
    )

    # Go get a BattleTag if required
    # if account.battletag == '':
    #     try:
    #         status, battletag = b.get_battle_tag()
    #         if status == 200:
    #             account.battletag = battletag['battletag']
    #             account.save(update_fields=['battletag'])
    #     except ValueError:
    #         pass

    # Now we can fetch WoW characters
    seen_chars = []
    status, profile = b.get_wow_profile()
    if status == 200:
        # Gather character realm/name pairs first
        char_filters = []
        for char_data in profile['characters']:
            if BAD_NAME_RE.match(char_data['name']):
                print('Skipping bad character: %r/%r/%r' % (account.region, char_data['realm'], char_data['name']))
                continue

            char_filters.append(Q(
                realm=manual_data.realm_map.get((account.region, char_data['realm'])),
                name=char_data['name'],
            ))

        if not char_filters:
            return

        char_map = {}
        #for character in Character.objects.filter(bnetaccount=account):
        for character in Character.objects.filter(functools.reduce(operator.or_, char_filters)):
            char_map[(character.realm_id, character.name)] = character

        guild_map = {}

        # {u'achievementPoints': 490,
        #  u'battlegroup': u'Cyclone',
        #  u'class': 8,
        #  u'gender': 1,
        #  u'guild': u'Drama n Gossipz',
        #  u'guildRealm': u'Darkspear',
        #  u'level': 60,
        #  u'name': u'Kiyuki',
        #  u'race': 5,
        #  u'realm': u'Darkspear',
        #  u'thumbnail': u'darkspear/108/32477548-avatar.jpg'},
        for char_data in profile['characters']:
            if BAD_NAME_RE.match(char_data['name']):
                print('Skipping bad character: %r/%r/%r' % (account.region, char_data['realm'], char_data['name']))
                continue

            if 'guild' in char_data:
                guild = guild_map.get((char_data['guildRealm'], char_data['guild']))
                if guild is None:
                    try:
                        guild = Guild.objects.select_related('realm').get(
                            realm__name=char_data['guildRealm'],
                            name=char_data['guild'],
                        )
                    except Guild.DoesNotExist:
                        guild_realm = manual_data.realm_map.get((account.region, char_data['guildRealm']))
                        if guild_realm is None:
                            print('BUG: strange realm %r/%r' % (account.region, char_data['guildRealm']))
                            continue

                        guild = Guild(
                            realm=manual_data.realm_map[(account.region, char_data['guildRealm'])],
                            name=char_data['guild'],
                        )
                        guild.save()
            else:
                guild = None

            realm = manual_data.realm_map.get((account.region, char_data['realm']))
            if realm is None:
                print('BUG: strange realm %r/%r' % (account.region, char_data['realm']))
                continue

            character = char_map.get((realm.id, char_data['name']))
            if character is None:
                char = Character(
                    bnetaccount=account,
                    name=char_data['name'],
                    realm=realm,
                    cls_id=char_data['class'],
                    race_id=char_data['race'],
                    gender=char_data['gender'],
                    level=char_data['level'],
                    thumbnail=char_data['thumbnail'],
                    guild=guild,
                )
                char.save()

            else:
                character.bnetaccount = account
                character.cls_id = char_data['class']
                character.race_id = char_data['race']
                character.gender = char_data['gender']
                character.level = char_data['level']
                character.thumbnail = char_data['thumbnail']
                character.guild = guild
                character.save(update_fields=['bnetaccount', 'cls', 'race', 'gender', 'level', 'thumbnail', 'guild'])

            seen_chars.append(
                Q(realm=realm.id)
                &
                Q(name=char_data['name'])
            )

        # Clean up any missing characters
        cleanup_qs = Character.objects.filter(bnetaccount=account)
        if seen_chars:
            cleanup_qs = cleanup_qs.exclude(functools.reduce(operator.or_, seen_chars))

        cleanup_qs.delete()

# ---------------------------------------------------------------------------

"""
from thing.views.oauth import _update_oauth
from core.models import BNetAccount
for bna in BNetAccount.objects.all():
  _update_oauth(bna)
"""
