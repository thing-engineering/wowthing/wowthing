from django.contrib.auth.decorators import login_required
from django.shortcuts import *
from django.urls import reverse

from core.models import Currency

from .home import home

# ---------------------------------------------------------------------------

@login_required
def settings_hide_currencies(request):
    if request.method == 'POST':
        profile = request.user.profile
        profile.hide_currencies = []
        currency_ids = set(Currency.objects.values_list('id', flat=True))

        for k, v in request.POST.items():
            if k.startswith('hide_') and v == 'on':
                try:
                    currency_id = int(k.split('_', 1)[1])
                except ValueError:
                    continue

                if currency_id in currency_ids:
                    profile.hide_currencies.append(currency_id)

        profile.save(update_fields=['hide_currencies'])

    return redirect(reverse(home, kwargs=dict(username=request.user.username)) + '#currencies')

# ---------------------------------------------------------------------------
