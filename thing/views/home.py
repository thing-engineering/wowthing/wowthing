from collections import OrderedDict

from django.contrib.auth.decorators import login_required
from django.forms import ModelForm
from django.shortcuts import *
from django.urls import reverse

from core.models import UserProfile
from core.util import get_public_user
from thing.models import Character

# ---------------------------------------------------------------------------

def home(request, username):
    public, user = get_public_user(request, username)
    if user is None:
        return public

    data = dict(
        public=public,
        public_user=public and user or None,
        anonymized=public and user.profile.anonymized,
        user_id=user and user.id or request.user.id,
        userdata_code=user.profile.code,
    )
    if request.user.is_authenticated and user == request.user:
        data['settings_form'] = SettingsForm(instance=request.user.profile)
        data['overview_group'] = user.profile.overview_group
        data['overview_sort'] = user.profile.overview_sort

    return render(
        request,
        'thing/home.html',
        data,
    )

# ---------------------------------------------------------------------------

@login_required
def home_redirect(request):
    return redirect(reverse(home, kwargs=dict(username=request.user.username)))

# ---------------------------------------------------------------------------

@login_required
def home_settings(request):
    if request.method == 'POST':
        profile = request.user.profile

        form = SettingsForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()

            # Update hide characters
            char_ids = set(Character.objects.filter(bnetaccount__user=request.user).values_list('id', flat=True))
            profile.hide_characters = []
            for k, v in request.POST.items():
                if k.startswith('hide_') and v == 'on':
                    try:
                        char_id = int(k.split('_', 1)[1])
                    except ValueError:
                        continue

                    if char_id in char_ids:
                        profile.hide_characters.append(char_id)

            profile.overview_show_currencies = [int(n) for n in request.POST.get('overview_show_currencies', '').split(',') if n.isdigit()]
            profile.overview_show_lockouts = [int(n) for n in request.POST.get('overview_show_lockouts', '').split(',') if n.isdigit()]

            # Update overview grouping
            parts = [p for p in request.POST.get('overview_group', '').split(',') if p in UserProfile.GROUP_TYPES] or ['default']
            profile.overview_group = ','.join(parts)

            # Update overview sort order
            parts = [p for p in request.POST.get('overview_sort', '').split(',') if p in UserProfile.SORT_TYPES] or ['default']
            profile.overview_sort = ','.join(parts)

            profile.save(update_fields=['hide_characters', 'overview_show_currencies', 'overview_show_lockouts', 'overview_group', 'overview_sort'])

    return redirect(reverse(home, kwargs=dict(username=request.user.username)))

# ---------------------------------------------------------------------------

class SettingsForm(ModelForm):
    dumb_fields = dict(
        public='Public: make your data visible to anyone',
        anonymized='Anomymize: anonymize public data when viewed by other people',
        show_in_leaderboard='Show your account name in public leaderboards',
    )

    def __init__(self, *args, **kwargs):
        super(SettingsForm, self).__init__(*args, **kwargs)

        # Override labels
        self.fields['overview_show_ail'].label = 'Overview show average item level'

        # Fix stupid fields
        self.sections = OrderedDict()
        for name, field in self.fields.items():
            if name in self.dumb_fields:
                section = 'General'
                field.label = self.dumb_fields[name]
            else:
                section, label = field.label.split(None, 1)
                field.label = label.capitalize()

            self.sections.setdefault(section, []).append(name)

    class Meta:
        model = UserProfile
        fields = [
            'public',
            'anonymized',
            'show_in_leaderboard',
            'gear_show_item_level',
            'gear_show_bags',
            'lockouts_ignore_old_expired_raids',
            'lockouts_ignore_current_expired_raids',
            'overview_show_totals',
            'overview_show_realm',
            'overview_show_race_icon',
            'overview_show_class_icon',
            'overview_show_rested',
            'overview_show_ail',
            'overview_show_played',
            'overview_show_gold',
            'overview_show_world_quests',
            'overview_show_honor',
        ]
