import functools
import operator

from django import forms
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.shortcuts import *
from django.urls import reverse

from core.models import Item
from thing.models import CharacterItem

# ---------------------------------------------------------------------------

GROUP_BY_CHOICES = (
    ('item', 'Item'),
    ('character', 'Character'),
)

# ---------------------------------------------------------------------------

class SearchForm(forms.Form):
    find = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={'class': 'form-control'}),
    )
    whole_words = forms.BooleanField(
        required=False,
        label="Whole words only",
    )
    quality = forms.ChoiceField(
        choices=(('', '-Any-'),) + Item.QUALITY_CHOICES,
        required=False,
        widget=forms.Select(attrs={'class': 'form-control'}),
    )
    location = forms.ChoiceField(
        choices=(('', '-Any-'),) + CharacterItem.LOCATION_CHOICES,
        required=False,
        widget=forms.Select(attrs={'class': 'form-control'}),
    )
    bind_type = forms.ChoiceField(
        choices=(('', '-Any'),) + Item.BIND_CHOICES,
        required=False,
        widget=forms.Select(attrs={'class': 'form-control'}),
        label="Bind type",
    )
    group_by = forms.ChoiceField(
        choices=GROUP_BY_CHOICES,
        required=False,
        widget=forms.Select(attrs={'class': 'form-control'}),
    )

# ---------------------------------------------------------------------------

@login_required
def search(request):
    filters = []
    items = []
    group_by = ''
    totals = {}

    if 'find' in request.GET:
        form = SearchForm(request.GET)
        if form.is_valid():
            # Item name
            for part in form.cleaned_data['find'].lower().split():
                if form.cleaned_data['whole_words']:
                    filters.append(
                        Q(item__lname=part)
                        |
                        Q(item__lname__startswith='%s ' % (part))
                        |
                        Q(item__lname__endswith=' %s' % (part))
                        |
                        Q(item__lname__contains=' %s ' % (part))
                    )
                else:
                    filters.append(Q(item__lname__contains=part))

            # Item quality
            if form.cleaned_data['quality']:
                filters.append(Q(item__quality=form.cleaned_data['quality']))

            # Item bind type
            if form.cleaned_data['bind_type']:
                filters.append(Q(item__bind_type=form.cleaned_data['bind_type']))

            # Item location
            if form.cleaned_data['location']:
                filters.append(Q(location=form.cleaned_data['location']))

            # Now filter items
            if filters:
                items = CharacterItem.objects.filter(
                    character__bnetaccount__user=request.user,
                ).filter(
                    functools.reduce(operator.and_, filters),
                ).prefetch_related(
                    'character__realm',
                    'item',
                )

                group_by = form.cleaned_data.get('group_by')
                # if group_by == 'item' or group_by == '':
                #     #items = items.order_by(
                #     #    'item__name',
                #     #    'character__realm__name',
                #     #    'character__name',
                #     #)

                #     for ci in items:

                # elif group_by == 'realm':
                #     #items = items.order_by(
                #     #    'character__realm__name',
                #     #    'item__name',
                #     #    'character__name',
                #     #)

                #     #for ci in items:
                #     #    totals[ci.character.realm.name] = totals.get(ci.character.realm.name, 0) + ci.count

                # Manual aggregation, fun
                temp = {}
                for ci in items:
                    key = (ci.character_id, ci.item_id, ci.item_level, ci.quality, ci.location)
                    if key not in temp:
                        temp[key] = [0, ci]
                    temp[key][0] = temp[key][0] + ci.count

                    if group_by == 'character':
                        totals[ci.character.realm.name] = totals.get(ci.character.realm.name, 0) + ci.count
                    else:
                        totals[ci.item.name] = totals.get(ci.item.name, 0) + ci.count

                if group_by == 'character':
                    sigh = [(ci.character.realm.name, ci.character.name, ci.item.name, count, ci) for count, ci in temp.values()]
                    sigh.sort()
                    items = [(s[3], s[4]) for s in sigh]
                else:
                    sigh = [(ci.item.name, ci.character.realm.name, ci.character.name, count, ci) for count, ci in temp.values()]
                    sigh.sort()
                    items = [(s[3], s[4]) for s in sigh]

    else:
        form = SearchForm()

    return render(
        request,
        'thing/search.html',
        dict(
            form=form,
            group_by=group_by,
            items=items,
            totals=totals,
            searched=filters and True,
        ),
    )

# ---------------------------------------------------------------------------
