import os

from .base import *

DEBUG = (os.getenv('PRODUCTION_DEBUG') is not None)

SECRET_KEY = os.environ['SECRET_KEY']

STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
STATIC_URL = '/static/'

TIME_ZONE = 'Etc/UTC'

ALLOWED_HOSTS = [
    'wowthing.org',
    'localhost', # testing
]

CSRF_COOKIE_SECURE = True
SESSION_COOKIE_SECURE = True
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': os.getenv('POSTGRES_DATABASE', 'wowthing'),
        'USER': os.getenv('POSTGRES_USER', ''),
        'PASSWORD': os.getenv('POSTGRES_PASS', ''),
        'HOST': os.getenv('POSTGRES_HOST', ''),
        'PORT': os.getenv('POSTGRES_PORT', ''),
        'CONN_MAX_AGE': 300,
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '%s:%s' % (os.getenv('MEMCACHED_HOST', ''), os.getenv('MEMCACHED_PORT', '11211')),
        'KEY_PREFIX': 'wowthing',
    }
}

BNET_REDIRECT_BASE = 'https://wowthing.org%s'

REDIS = (os.getenv('REDIS_HOST'), int(os.getenv('REDIS_PORT', '6379')), int(os.getenv('REDIS_DATABASE', 0)))

EMAIL_HOST = 'smtp-relay.things.svc.cluster.local'
EMAIL_PORT = 25
