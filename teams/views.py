import json

from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.db.models import Max
from django.shortcuts import *
from django.urls import reverse

from core.models import Realm
from teams.models import *
from thing.models import Character, CharacterItem, CharacterMythicPlus

# ---------------------------------------------------------------------------

@login_required
def create(request):
    if not request.user.profile.raidgroups_enabled:
        raise PermissionDenied()

    if request.method == 'POST':
        team = Team(
            owner_id=request.user.id,
            region=request.POST.get('region', 'us'),
            name=request.POST.get('name', 'Unnamed raid group'),
            description=request.POST.get('description', ''),
        )
        default_realm = request.POST.get('default_realm', '')
        if default_realm.isdigit():
            team.default_realm_id = int(default_realm)
        team.generate_access_key()
        team.save()

        return redirect(reverse('teams:view', kwargs=dict(key=team.access_key)))

    return render(
        request,
        'teams/create.html',
    )

# ---------------------------------------------------------------------------

@login_required
def add_character(request):
    team = get_object_or_404(Team, pk=request.POST['team'])
    if team.owner == request.user:
        char = None
        char_name = request.POST['character'][:16].split()[0]
        try:
            char = Character.objects.get(realm=request.POST['realm'], name=char_name)
        except Character.DoesNotExist:
            char = Character(
                bnetaccount=None,
                guild=None,
                name=request.POST['character'][:16],
                realm_id=request.POST['realm'],
                level=10,
                race_id=7,# Gnome
                cls_id=3, # Hunter
                last_api_update=None,
            )
            char.save()

        try:
            rgm = TeamMember.objects.get(team=team, character=char)
        except TeamMember.DoesNotExist:
            rgm = TeamMember(
                team=team,
                character=char,
                primary_role=0,
                secondary_role=0,
                note=request.POST['note'][:64],
            )
            rgm.save()

    return redirect(reverse('teams:view', kwargs=dict(key=team.access_key)))

# ---------------------------------------------------------------------------

@login_required
def del_character(request, team_id, member_id):
    team = get_object_or_404(Team, pk=team_id)

    if team.owner == request.user:
        team.members.filter(pk=member_id).delete()

    return redirect(reverse('teams:view', kwargs=dict(key=team.access_key)))

# ---------------------------------------------------------------------------

@login_required
def edit_roles(request, team_id):
    team = get_object_or_404(Team, pk=team_id)

    if team.owner == request.user and request.method == 'POST':
        member_map = {}
        for member in TeamMember.objects.filter(team=team):
            member_map[member.id] = member

        chars = request.POST.get('changes', '').split(';')
        for char in chars:
            parts = [int(p) for p in char.split(',') if p.isdigit()]
            if len(parts) == 3 and parts[0] in member_map:
                member = member_map[parts[0]]
                if parts[1] in range(5):
                    member.primary_role = parts[1]
                if parts[2] in range(5):
                    member.secondary_role = parts[2]
                member.save(update_fields=['primary_role', 'secondary_role'])

    return redirect(reverse('teams:view', kwargs=dict(key=team.access_key)))

# ---------------------------------------------------------------------------

def view(request, key):
    team = get_object_or_404(Team, access_key=key)

    # Fetch data
    characters = {}
    members = []
    for rgm in team.members.prefetch_related('character').order_by('character__name'):
        char = rgm.character
        
        members.append(dict(
            id=rgm.id,
            primary_role=rgm.primary_role,
            secondary_role=rgm.secondary_role,
            note=rgm.note,
            character=dict(
                id=char.id,
                level=char.level,
                name=char.name,
                realm_id=char.realm_id,
                cls=char.cls_id,
                race=char.race_id,
                gender=char.gender,
                items=[],
                mythicplus={},
                azerite_level=char.azerite_level,
                keystone_max=char.keystone_max,
                keystone_period=char.keystone_period,
            )
        ))
        characters[members[-1]['character']['id']] = members[-1]['character']

    # bulk items
    ci_qs = CharacterItem.objects.filter(
        character__in=characters.keys(),
        location=CharacterItem.EQUIPPED_LOCATION,
    ).prefetch_related(
        'item',
    )
    
    items = {}
    for ci in ci_qs:
        characters[ci.character_id]['items'].append(dict(
            s=ci.slot,
            i=ci.item_id,
            l=ci.item_level or ci.item.item_level(characters[ci.character_id]['level']),
            q=ci.quality,
            c=ci.context,
            e=ci.extra,
        ))

        if ci.item_id not in items:
            items[ci.item_id] = dict(
                c=ci.item.cls,
                i=ci.item.icon,
                u=ci.item.upgrades,
            )

    # bulk mythic plus
    season_max = CharacterMythicPlus.objects.all().aggregate(Max('season'))['season__max']

    cmp_qs = CharacterMythicPlus.objects.filter(
        character__in=characters.keys(),
        season=season_max,
    ).order_by(
        '-in_time',
    )
    
    for cmplus in cmp_qs:
        characters[cmplus.character_id]['mythicplus'].setdefault(cmplus.dungeon_id, []).append(dict(
            level=cmplus.level,
            completed_at=cmplus.completed_at,
            duration=cmplus.duration,
            in_time=cmplus.in_time,
            affixes=cmplus.affixes,
        ))

    #members.sort(key=lambda m: (m['primary_role'], m['character']['name']))

    data = dict(
        items=items,
        members=members,
        team_id=team.id,
        region=team.region,
        default_realm=team.default_realm_id,
        owner=request.user.is_authenticated and request.user == team.owner,
    )

    return render(
        request,
        'teams/view.html',
        dict(
            json_data=json.dumps(data),
            team=team,
        ),
    )

# ---------------------------------------------------------------------------
