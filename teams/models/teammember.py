from django.db import models

# ---------------------------------------------------------------------------

class TeamMember(models.Model):
    TANK_ROLE = 1
    HEAL_ROLE = 2
    MELEE_ROLE = 3
    RANGED_ROLE = 4

    ROLE_CHOICES = (
        (0, '-None-'),
        (1, 'Tank'),
        (2, 'Healer'),
        (3, 'Melee DPS'),
        (4, 'Ranged DPS'),
    )

    team = models.ForeignKey('teams.Team', related_name='members', on_delete=models.CASCADE)
    character = models.ForeignKey('thing.Character', related_name='teams', on_delete=models.CASCADE)

    primary_role = models.SmallIntegerField(choices=ROLE_CHOICES)
    secondary_role = models.SmallIntegerField(choices=ROLE_CHOICES)
    note = models.CharField(max_length=64)

# ---------------------------------------------------------------------------
